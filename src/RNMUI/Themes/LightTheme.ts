const LightTheme = {
  color: {
    light: '#FFFFFF',
    shadow: '#D8D8D8',
    danger: '#DD1C1A',
    primary: '#0582CA',
    warning: '#F0C808',
    dark: '#1B1B1E',
    overlay: 'rgba(0,0,0,0.4)',
    white: '#ffffff',
    black: '#000000'
  },
  statusbar: {
    style: 'dark-content',
    backgroundColor: '#1B1B1E'
  }
};

export default LightTheme;
