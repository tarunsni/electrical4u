const DarkTheme = {
  color: {
    dark: '#FFFFFF',
    shadow: '#353535',
    danger: '#DD1C1A',
    primary: '#0582CA',
    warning: '#F0C808',
    light: '#222222',
    overlay: 'rgba(0,0,0,0.4)',
    white: '#ffffff',
    black: '#000000'
  },
  statusbar: {
    style: 'light-content',
    backgroundColor: '#FFFFFF'
  }
};

export default DarkTheme;
