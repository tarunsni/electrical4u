import React from 'react';
import LightTheme from './LightTheme';
import DarkTheme from './DarkTheme';

export const ThemeContext = React.createContext<any | null>(LightTheme);

export const ThemeProvider = ThemeContext.Provider;

export const ThemeConsumer = ThemeContext.Consumer;
