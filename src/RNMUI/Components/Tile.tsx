import React, { StatelessComponent, useContext } from 'react';
import {
  ViewStyle,
  View,
  ImageBackground,
  ImageBackgroundProps,
  ViewProps,
  ImageStyle
} from 'react-native';
import { ThemeContext } from '..';

interface TileProps {
  style?: ViewStyle;
  overlayStyle?: ViewStyle;
  imageStyle?: ImageStyle;
  children?: any;
  imageProps: ImageBackgroundProps;
  overlayProps?: ViewProps;
  tileProps?: ViewProps;
  hideOverlay?: boolean;
}

const Tile: StatelessComponent<TileProps> = props => {
  const {
    style,
    children,
    overlayStyle,
    imageProps,
    hideOverlay,
    tileProps,
    overlayProps,
    imageStyle
  } = props;
  const theme = useContext(ThemeContext);
  const { color } = theme;

  function overlay(children: any, hideOverlay: boolean) {
    let overlayFlagStyle;
    const commonStyle: any = {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      ...overlayStyle
    };
    if (hideOverlay) {
      overlayFlagStyle = {
        ...commonStyle,
        backgroundColor: 'transparent'
      };
    } else {
      overlayFlagStyle = {
        backgroundColor: color.overlay,
        ...commonStyle
      };
    }
    return (
      <View style={overlayFlagStyle} {...overlayProps}>
        {children}
      </View>
    );
  }

  function imageBackground(content: any) {
    return (
      <ImageBackground
        style={{
          flex: 1,
          ...imageStyle
        }}
        {...imageProps}
      >
        {content}
      </ImageBackground>
    );
  }

  function setUpTile() {
    if (hideOverlay) {
      return imageBackground(overlay(children, true));
    } else {
      return imageBackground(overlay(children, false));
    }
  }

  return (
    <View
      style={{ flex: 1, backgroundColor: color.light, ...style }}
      {...tileProps}
    >
      {setUpTile()}
    </View>
  );
};

Tile.defaultProps = {
  hideOverlay: false
};

export default Tile;
