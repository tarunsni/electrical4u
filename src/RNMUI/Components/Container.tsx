import React, { StatelessComponent, useContext, Fragment } from 'react';
import {
  View,
  Platform,
  SafeAreaView,
  ViewStyle,
  StatusBar,
  ViewProps
} from 'react-native';
import { ThemeContext } from '..';

interface ContainerProps {
  style?: ViewStyle;
  children?: any;
  containerProps?: ViewProps;
  statusBarStyle?: string;
  statusBarBackgroundColor?: string;
}

const Container: StatelessComponent<ContainerProps> = props => {
  const {
    style,
    children,
    containerProps,
    statusBarStyle,
    statusBarBackgroundColor
  } = props;

  const theme = useContext(ThemeContext);
  const { color, statusbar } = theme;

  function iOS() {
    return (
      <Fragment>
        <SafeAreaView
          style={{
            flex: 0,
            backgroundColor: statusBarBackgroundColor || color.light
          }}
        />
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: color.light,
            ...style
          }}
          {...containerProps}
        >
          {children}
        </SafeAreaView>
      </Fragment>
    );
  }

  function android() {
    return (
      <View style={{ flex: 1, ...style }} {...containerProps}>
        {children}
      </View>
    );
  }

  return (
    <Fragment>
      <StatusBar
        barStyle={statusBarStyle || statusbar.style}
        backgroundColor={statusBarBackgroundColor || statusbar.backgroundColor}
      />
      {Platform.OS === 'ios' ? iOS() : android()}
    </Fragment>
  );
};

export default Container;
