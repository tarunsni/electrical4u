import React, { StatelessComponent, useContext } from 'react';
import { ViewStyle, Platform, View, ViewProps } from 'react-native';
import { ThemeContext } from '..';

interface HeaderProps {
  style?: ViewStyle;
  children?: any;
  type?: 'default' | 'transparent' | 'noBorder';
  elevation?: number;
  headerProps?: ViewProps;
}

const Header: StatelessComponent<HeaderProps> = props => {
  const { style, children, type, headerProps } = props;
  const height = Platform.OS === 'ios' ? 56 : 65;
  const theme = useContext(ThemeContext);
  const { color } = theme;

  const defaultHeader = () => (
    <View
      style={{
        height,
        backgroundColor: color.light,
        ...style,
        borderBottomWidth: 2,
        borderBottomColor: color.shadow
      }}
      {...headerProps}
    >
      {children}
    </View>
  );
  const transparentHeader = () => (
    <View style={{ height, ...style, backgroundColor: 'transparent' }}>
      {children}
    </View>
  );
  const noBorderHeader = () => (
    <View style={{ height, backgroundColor: color.light, ...style }}>
      {children}
    </View>
  );

  const switchHeader =
    type === 'default'
      ? defaultHeader()
      : type === 'transparent'
      ? transparentHeader()
      : type === 'noBorder'
      ? noBorderHeader()
      : defaultHeader();

  return switchHeader;
};

Header.defaultProps = {
  type: 'default',
  elevation: 3
};

export default Header;
