import React, { StatelessComponent } from 'react';
import { Text } from 'react-native';

interface SpacerProps {
  size?: number;
}

const Spacer: StatelessComponent<SpacerProps> = props => {
  const { size } = props;

  return <Text style={{ fontSize: size }}> </Text>;
};

Spacer.defaultProps = {
  size: 8
};

export default Spacer;
