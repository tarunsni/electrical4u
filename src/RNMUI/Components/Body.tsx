import React, { StatelessComponent, useContext } from 'react';
import { ScrollView, ViewStyle, ScrollViewProps } from 'react-native';
import { ThemeContext } from '..';

interface BodyProps {
  style?: ViewStyle;
  children?: any;
  bodyProps?: ScrollViewProps;
}

const Body: StatelessComponent<BodyProps> = props => {
  const { style, children, bodyProps } = props;
  const theme = useContext(ThemeContext);
  const { color } = theme;
  return (
    <ScrollView
    keyboardShouldPersistTaps='always'
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: color.light,
        ...style
      }}
      {...bodyProps}
    >
      {children}
    </ScrollView>
  );
};

export default Body;
