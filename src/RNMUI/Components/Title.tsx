import React, { StatelessComponent, useContext } from 'react';
import { Text, TextStyle, TextProps } from 'react-native';
import { ThemeContext } from '..';

interface TitleInterface {
  children?: any;
  style?: TextStyle;
  size?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
  titleProps?: TextProps;
}

const Title: StatelessComponent<TitleInterface> = props => {
  const { children, size, style, titleProps } = props;

  const theme = useContext(ThemeContext);
  const { color } = theme;

  const sizeChoozer = () => {
    let textSize;
    switch (size) {
      case 'h1':
        textSize = 16 * 2;
        break;
      case 'h2':
        textSize = 16 * 1.5;
        break;
      case 'h3':
        textSize = 16 * 1.17;
        break;
      case 'h4':
        textSize = 16;
        break;
      case 'h5':
        textSize = 16 * 0.83;
        break;
      case 'h6':
        textSize = 16 * 0.67;
        break;
      default:
        textSize = 16 * 1.17;
        break;
    }
    return textSize;
  };

  return (
    <Text
      style={{
        fontSize: sizeChoozer(),
        fontWeight: 'bold',
        color: color.dark,
        ...style
      }}
      {...titleProps}
    >
      {children}
    </Text>
  );
};

export default Title;
