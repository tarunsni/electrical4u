import React, { useContext } from 'react';
import {
  View,
  Platform,
  TouchableNativeFeedback,
  TouchableNativeFeedbackProps,
  TouchableOpacityProps,
  TouchableOpacity
} from 'react-native';
import { ThemeContext } from '..';

interface ButtonInterface {
  children?: any;
  buttonProps?: TouchableNativeFeedbackProps | TouchableOpacityProps;
  style?: any;
  onPress: Function;
  height: number;
  width: number;
  type?: 'primary' | 'white' | 'danger' | 'warning' | 'black' | 'shadow';
}

const Button = (props: ButtonInterface) => {
  const { buttonProps, style, children, height, width, onPress, type } = props;
  const theme = useContext(ThemeContext);
  const { color } = theme;

  function buttonContent() {
    return (
      <View
        style={{
          backgroundColor: color[type || 'primary'],
          borderRadius: 5,
          width: width,
          height: height,
          justifyContent: 'center',
          alignItems: 'center',
          ...style
        }}
      >
        {children}
      </View>
    );
  }

  function ios() {
    return (
      <TouchableOpacity
        style={{ width: '100%' }}
        activeOpacity={0.5}
        onPress={() => onPress()}
        {...buttonProps}
      >
        {buttonContent()}
      </TouchableOpacity>
    );
  }

  function android() {
    return (
      <TouchableNativeFeedback
        style={{ width: '100%' }}
        useForeground
        background={TouchableNativeFeedback.Ripple(color.light)}
        onPress={() => onPress()}
        {...buttonProps}
      >
        {buttonContent()}
      </TouchableNativeFeedback>
    );
  }

  return Platform.OS === 'ios' ? ios() : android();
};

export default Button;
