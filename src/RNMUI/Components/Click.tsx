import React, { useContext } from 'react';
import {
  View,
  Platform,
  TouchableNativeFeedback,
  TouchableNativeFeedbackProps,
  TouchableOpacityProps,
  TouchableOpacity,
  ViewStyle
} from 'react-native';
import { ThemeContext, Card } from '..';

interface ClickInterface {
  children?: any;
  clickProps?: TouchableNativeFeedbackProps | TouchableOpacityProps;
  style?: ViewStyle;
  onPress: Function;
}

const Click = (props: ClickInterface) => {
  const { clickProps, style, children, onPress } = props;
  const theme = useContext(ThemeContext);
  const { color } = theme;

  function clickContent() {
    return (
      <View
        style={{
          ...style
        }}
      >
        {children}
      </View>
    );
  }

  function ios() {
    return (
      <TouchableOpacity
        style={{ flex: 1 }}
        activeOpacity={0.5}
        onPress={() => onPress()}
        {...clickProps}
      >
        {clickContent()}
      </TouchableOpacity>
    );
  }

  function android() {
    return (
      <TouchableNativeFeedback
        style={{ flex: 1 }}
        useForeground
        background={TouchableNativeFeedback.Ripple(color.light)}
        onPress={() => onPress()}
        {...clickProps}
      >
        {clickContent()}
      </TouchableNativeFeedback>
    );
  }

  return Platform.OS === 'ios' ? ios() : android();
};

export default Click;
