import React, { StatelessComponent, useContext } from 'react';
import { ViewStyle, ViewProps } from 'react-native';
import ElevatedView from '../Config/Library/ElevatedView';
import { ThemeContext } from '..';

interface CardProps {
  style?: ViewStyle;
  children?: any;
  elevation?: number;
  rounded?: boolean;
  roundness?: number;
  cardProps?: ViewProps;
  transparent?: boolean;
}

const Card: StatelessComponent<CardProps> = props => {
  const {
    style,
    children,
    elevation,
    rounded,
    roundness,
    cardProps,
    transparent
  } = props;
  const theme = useContext(ThemeContext);
  const { color } = theme;

  return (
    <ElevatedView
      elevation={elevation}
      style={{
        backgroundColor: transparent ? 'transparent' : color.shadow,
        borderRadius: rounded ? roundness : 0,
        ...style
      }}
      {...cardProps}
    >
      {children}
    </ElevatedView>
  );
};

Card.defaultProps = {
  elevation: 3,
  roundness: 5
};

export default Card;
