import { createStackNavigator } from 'react-navigation';
import AccountChooserScreen from '../Screens/AccountChooserScreen';
import RegisterScreen from '../Screens/RegisterScreen';

const RegisterStack = createStackNavigator(
  {
    Account: {
      screen: AccountChooserScreen
    },
    Register: {
      screen: RegisterScreen
    }
  },
  {
    initialRouteName: 'Account',
    headerMode: 'none'
  }
);

export default RegisterStack;
