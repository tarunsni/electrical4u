import { createAppContainer, createStackNavigator } from 'react-navigation';
import TabNavigation from './TabNavigation';
import LoginStack from './LoginStack';
import LoginScreen from '../Screens/LoginScreen';

const RootStackNavigation = createStackNavigator(
  {
    Initial: {
      screen: TabNavigation
    },
    Login: {
      screen: LoginScreen
    }
  },
  {
    initialRouteName: 'Initial',
    headerMode: 'none'
  }
);

export default createAppContainer(RootStackNavigation);
