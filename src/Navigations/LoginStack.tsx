import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../Screens/LoginScreen';
import ForgetPasswordScreen from '../Screens/ForgetPasswordScreen';
import HomeScreen from '../Screens/HomeScreen';

const LoginStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Forget: {
      screen: ForgetPasswordScreen,
      navigationOptions: {
        tabBarVisible: false
      }
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  }
);

LoginStack.navigationOptions = ({ navigation }: any) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

export default LoginStack;
