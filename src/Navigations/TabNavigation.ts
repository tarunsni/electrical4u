import { createMaterialTopTabNavigator } from 'react-navigation';
import Utility from '../Utilities/Utility';
import RegisterStack from './RegisterStack';
import { Platform } from 'react-native';
import Theme from '../Utilities/Theme';
import { verticalScale } from 'react-native-size-matters';
import LoginStack from './LoginStack';

const TabNavigation = createMaterialTopTabNavigator(
  {
    LoginStack: {
      screen: LoginStack,
      navigationOptions: {
        title: 'Sign In'
      }
    },
    RegisterStack: {
      screen: RegisterStack,
      navigationOptions: {
        title: 'Register'
      }
    }
  },
  {
    initialRouteName: 'LoginStack',
    tabBarOptions: {
      style: {
        backgroundColor: Theme.color.light,
        height: Platform.OS == 'ios' ? Utility.getHeight(160 / 1334) : 'auto',
        justifyContent: 'flex-end',
        paddingBottom: Utility.getHeight(16 / 1334)
      },
      activeTintColor: Theme.color.primary,
      inactiveTintColor: Theme.color.darkShadow,
      indicatorStyle: {
        backgroundColor: Theme.color.primary
      },
      labelStyle: {
        fontFamily: Theme.font.bold,
        fontWeight: Platform.OS == 'ios' ? 'bold' : '200',
        fontSize: verticalScale(18),
        marginBottom: Platform.OS == 'ios' ? verticalScale(-5) : 0
      }
    }
  }
);

export default TabNavigation;
