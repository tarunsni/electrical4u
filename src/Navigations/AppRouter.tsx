import React, { Component } from 'react';
import RootStackNavigation from './RootStackNavigation';
import SplashScreen from 'react-native-splash-screen';
import AuthStackNavigation from './AuthStackNavigation';
import DataStore from '../Utilities/DataStore';
import LoadingScreen from '../Components/Shared/LoadingScreen';
import { connect } from 'react-redux';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import { NavigationScreenProp } from 'react-navigation';
import Utility from '../Utilities/Utility';
import { loginUser } from '../Redux/ActionCreators/loginAction';

interface AppRouterInterface {
  navigation: NavigationScreenProp<any>;
  appState: any;
  updateAppState: Function;
  loginUser: Function;
}

class AppRouter extends Component<AppRouterInterface> {
  async componentDidMount() {
    const { updateAppState } = this.props;
    const auth0Token = await DataStore.get('auth0Token');

    if (auth0Token) {
      const userData = await DataStore.get('userData');
      if (userData) {
        const userDataParsed = Utility.JSONParse(userData);
        updateAppState('userData', userDataParsed);
        updateAppState('loggedInStatus', 1);
      } else {
        await DataStore.remove('auth0Token');
        updateAppState('loggedInStatus', 2);
      }
    } else {
      updateAppState('loggedInStatus', 2);
    }
    setTimeout(() => {
      SplashScreen.hide();
    }, 300);
  }

  render() {
    const { loggedInStatus } = this.props.appState;
    return loggedInStatus === 0 ? (
      <LoadingScreen />
    ) : loggedInStatus === 1 ? (
      <AuthStackNavigation />
    ) : (
      <RootStackNavigation />
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.registerReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  loginUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppRouter);
