import { createAppContainer, createStackNavigator } from 'react-navigation';
import HomeScreen from '../Screens/HomeScreen';
import SettingsScreen from '../Screens/SettingsScreen';
import ProfileScreen from '../Screens/ProfileScreen';
import EditProfileScreen from '../Screens/EditProfileScreen';
import ChangePasswordScreen from '../Screens/ChangePasswordScreen';
import QuestionScreen from '../Screens/QuestionScreen';
import ContactUsScreen from '../Screens/ContactUsScreen';

const AuthStackNavigation = createStackNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    Settings: {
      screen: SettingsScreen
    },
    Profile: {
      screen: ProfileScreen
    },
    EditProfile: {
      screen: EditProfileScreen
    },
    ChangePassword: {
      screen: ChangePasswordScreen
    },
    ContactUs: {
      screen: ContactUsScreen
    },
    Question: {
      screen: QuestionScreen
    }
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none'
  }
);

export default createAppContainer(AuthStackNavigation);
