import { Platform } from 'react-native';

interface ColorInterface {
  light: string;
  shadow: string;
  danger: string;
  primary: string;
  warning: string;
  dark: string;
  overlay: string;
  lightOverlay: string;
  white: string;
  black: string;
  lightShadow: string;
  darkShadow: string;
  darkerShadow: string;
  borderPink: string;
  lightPink: string;
  lightGreen: string;
  borderGreen: string;
  success: string;
}

interface ThemeInterface {
  color: ColorInterface;
  statusbar: any;
  font: {
    regular: string;
    bold: string;
  };
}

const Theme: ThemeInterface = {
  color: {
    light: '#f3f3f3',
    shadow: '#D8D8D8',
    danger: '#ff2826',
    primary: '#998455',
    warning: '#F0C808',
    dark: '#1B1B1E',
    overlay: 'rgba(0,0,0,0.4)',
    lightOverlay: 'rgba(0,0,0,0.2)',
    white: '#ffffff',
    black: '#000000',
    lightShadow: '#b4b4b4',
    darkShadow: '#8e8e8e',
    darkerShadow: '#6e6e6e',
    borderPink: '#fe6f81',
    lightPink: '#fed3d8',
    lightGreen: '#cbfde0',
    borderGreen: '#6dd9a1',
    success: '#29be6c'
  },
  statusbar: {
    style: 'dark-content',
    backgroundColor: '#f3f3f3'
  },
  font: {
    regular: Platform.OS == 'ios' ? 'NexaRegular' : 'Nexa Regular',
    bold: Platform.OS == 'ios' ? 'Nexa-Bold' : 'Nexa Bold'
  }
};

export default Theme;
