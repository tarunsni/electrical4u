import AsyncStorage from "@react-native-community/async-storage";

class DataStore {
  async store(key: string, data: string) {
    return await AsyncStorage.setItem(key, data);
  }

  async get(key: string) {
    return await AsyncStorage.getItem(key);
  }

  async remove(key: string) {
    return await AsyncStorage.removeItem(key);
  }

  async delete() {
    return await AsyncStorage.clear();
  }
}

export default new DataStore();
