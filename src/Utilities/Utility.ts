import { Dimensions, StatusBar, Platform } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import Auth0 from 'react-native-auth0';
import Snackbar from 'react-native-snackbar';

class Utility {
  navigation: any;
  auth0: Auth0;

  constructor() {
    this.auth0 = new Auth0({
      domain: 'dev-whlxe8a0.auth0.com',
      clientId: '9Dc8ptw2HZ4mnLw_3IomaeZ9uv6nVIsd'
    });
  }

  async loginWithAuth0() {
    return await this.auth0.webAuth.authorize({
      scope: 'openid offline_access email profile',
      audience: 'https://dev-whlxe8a0.auth0.com/userinfo',
      prompt: 'login'
    });
  }

  async loginWithAuth0Realm(user: any) {
    return await this.auth0.auth.passwordRealm({
      scope: 'offline_access',
      realm: 'Username-Password-Authentication',
      ...user
    });
  }

  async resetPassword(email: string) {
    return await this.auth0.auth.resetPassword({
      connection: 'Username-Password-Authentication',
      email: email
    });
  }

  async registerWithAuth0Realm(user: any) {
    return await this.auth0.auth.createUser({
      connection: 'Username-Password-Authentication',
      ...user
    });
  }

  async getUser(token: string) {
    return await this.auth0.auth.userInfo({ token: token });
  }

  async logout() {
    return await this.auth0.auth.logoutUrl({
      clientId: '9Dc8ptw2HZ4mnLw_3IomaeZ9uv6nVIsd',
      federated: true
    });
  }

  setNavigation(navigation: NavigationScreenProp<any>) {
    this.navigation = navigation;
  }

  getNavigation(): NavigationScreenProp<any> {
    return this.navigation;
  }

  getHeight(ratio: number, considerStatusBarHeight?: boolean) {
    return (
      Dimensions.get('screen').height * ratio -
      (considerStatusBarHeight
        ? this.getStatusBarHeight()
        : Platform.OS == 'ios'
        ? this.getStatusBarHeight()
        : 0)
    );
  }

  getWidth(ratio: number) {
    return Dimensions.get('window').width * ratio;
  }

  getStatusBarHeight() {
    return StatusBar.currentHeight ? StatusBar.currentHeight : 0;
  }

  showErrorToast(message: string) {
    this.toast(message, '#FE5E41');
  }

  showSuccessToast(message: string) {
    this.toast(message, '#17AF84');
  }

  showWarningToast(message: string) {
    this.toast(message, '#F7DD72');
  }

showNormalToast(message: string) {
    this.toast(message, '#222222');
  }

  toast(message: string, backgroundColor: string) {
    Snackbar.show({
      title: message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: backgroundColor
    });
  }

  JSONParse(data: string) {
    try {
      return JSON.parse(data);
    } catch (error) {
      return data;
    }
  }

  errorHandler(error: any, dispatch: Function) {
    console.log(error);
    console.log(error.response.data);
    if (error.response) {
      if (
        error.response.status === 404 ||
        error.response.status === 403 ||
        error.response.status === 409
      ) {
        if (error.response.data) {
          this.showErrorToast(error.response.data.message);
        }
      }
    } else {
      this.showErrorToast('Failed to connect with server');
    }
  }
}

export default new Utility();
