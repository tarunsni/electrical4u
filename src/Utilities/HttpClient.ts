import axios from 'axios';

class HttpClient {
  api_url: string;
  base_url: string = 'https://www.electrical4u.com/';

  constructor() {
    this.api_url =
      'http://spicaworks.com.md-94.webhostbox.net/e4u/index.php/api/';
  }

  generateHeader() {
    const headers = {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    };
    return headers;
  }

  //   generateAuthenticatedHeader() {
  //     const userData = JSON.parse(DataStore.get('userData'));
  //     this.headers = {
  //       headers: {
  //         'Content-Type': 'application/json;charset=UTF-8',
  //         'X-Token': userData ? userData.token : null
  //       }
  //     };
  //     return this.headers;
  //   }

  async register(body: any) {
    return await axios.post(this.api_url + 'signup', body);
  }

  async login(body: any) {
    return await axios.post(this.api_url + 'login', body);
  }

  async forgetPassword(body: any) {
    return await axios.post(this.api_url + 'forgetPassword', body);
  }

  async updateProfile(id: string, body: any) {
    return await axios.put(this.api_url + `editProfile/${id}`, body);
  }

  async getQuestions(pageNo: number, body: any) {
    return await axios.put(this.api_url + `questions/${pageNo}`, body);
  }

  async subjectList() {
    return await axios.get(this.api_url + `subjectList`, this.generateHeader());
  }
}

export default new HttpClient();
