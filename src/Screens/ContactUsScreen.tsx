import React, { Component } from 'react';
import { Container, Body, Header, Card, Click } from '../RNMUI';
import { connect } from 'react-redux';
import Theme from '../Utilities/Theme';
import { View, Image, Platform, ActivityIndicator } from 'react-native';
import { TextComponent, ContactUsForm } from '../Components';
import { verticalScale } from 'react-native-size-matters';
import Utility from '../Utilities/Utility';
import { NavigationScreenProp } from 'react-navigation';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import { updateUserProfile } from '../Redux/ActionCreators/profileAction';
import { updateState } from '../Redux/ActionCreators/contactUsAction';

interface ContactUsScreenInterface {
  navigation: NavigationScreenProp<any>;
  updateAppState: Function;
  appState: any;
  state: any;
  updateState: Function;
}

class ContactUsScreen extends Component<ContactUsScreenInterface> {
  state = {
    userData: {}
  };

  header() {
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.goBack()}
            >
              <Image
                source={require('../Assets/back_arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(15),
                  width: verticalScale(21)
                }}
              />
            </Click>
          </View>

          <View
            style={{
              flex: 8,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              Contact Us
            </TextComponent>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          />
        </View>
      </Header>
    );
  }

  componentDidMount() {
    const { userData } = this.props.appState;
    const { email, userName, phoneNumber } = userData;
    this.props.updateState('email', email);
    this.props.updateState('userName', userName);
    this.props.updateState('phoneNumber', phoneNumber);
  }

  render() {
    const { loading } = this.props.state;
    return (
      <Container
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
      >
        {this.header()}
        <Body>
          <View
            style={{
              height: Utility.getHeight(65 / 1334),
              justifyContent: 'center',
              alignItems: 'center'
            }}
          />
          <View style={{ paddingHorizontal: Utility.getWidth(25 / 750) }}>
            <Card
              style={{
                borderRadius: 5,
                overflow: 'hidden'
              }}
              elevation={5}
            >
              <ContactUsForm
                userData={this.props.state}
                updateUserData={this.props.updateState}
              />
            </Card>
          </View>
          <View
            style={{
              height: Utility.getHeight(85 / 1334)
            }}
          />
          <View
            style={{
              paddingHorizontal: Utility.getWidth(25 / 750),
              flexDirection: 'row'
            }}
          >
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
              <Click
                onPress={() => {}}
                style={{
                  height: Utility.getHeight(80 / 1334),
                  width: Utility.getWidth(250 / 750),
                  backgroundColor: Theme.color.primary,
                  borderRadius: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1
                }}
              >
                {loading ? (
                  <ActivityIndicator color={Theme.color.light} size={'large'} />
                ) : (
                  <TextComponent
                    fontFamily={Theme.font.bold}
                    style={{ color: Theme.color.white }}
                    iosMargin
                  >
                    Submit
                  </TextComponent>
                )}
              </Click>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }} />
          </View>
        </Body>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.contactReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  updateState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactUsScreen);
