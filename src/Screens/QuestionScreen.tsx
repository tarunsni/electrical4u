import React, { Component, Fragment } from 'react';
import { withNavigationFocus, NavigationScreenProp, FlatList, } from 'react-navigation';
import { connect } from 'react-redux';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import { View, Platform, Image, Modal,NetInfo,} from 'react-native';
import { Container, Header, Click, Body, Card, } from '../RNMUI';
import Theme from '../Utilities/Theme';
import { verticalScale } from 'react-native-size-matters';
import Swiper from 'react-native-swiper';
import { TextComponent } from '../Components';
import Utility from '../Utilities/Utility';
import {
  updateState,
  getQuestions
} from '../Redux/ActionCreators/questionActions';
import LoadingScreen from '../Components/Shared/LoadingScreen';
import NoInternetView from '../Components/Shared/NoInternetView';
import { WebView } from 'react-native-webview';
import QuestionComponent from '../Components/Layout/QuestionComponent';
import QuestionRenderer from '../Components/Layout/QuestionRenderer';

interface QuestionScreenInterface {
  navigation: NavigationScreenProp<any>;
  state: any;
  updateState: Function;
  getQuestions: Function;
}


class QuestionScreen extends Component<QuestionScreenInterface> {
  state = {
    modalOpen: false,
    modalText: '',
    questions_local:[],
    isConnected:true,
  };
  handler: any;

  componentWillMount(){
    this.addListener()
  }
  componentDidMount() {
    const subjectName = this.props.navigation.getParam('subjectName');
    this.props.getQuestions(1, { subject: subjectName });
  }

  componentWillUnmount() {
    this.props.updateState('currentQuestion', 1);
    this.setState({ modalOpen: false, modalText: '' });
    this.removeConnectionListener()
  }

  removeConnectionListener(){
    NetInfo.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange,
    );
  }

  addListener(){
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange
  ); 
  NetInfo.isConnected.fetch().then((isConnected:any) => {
    this.setState({isConnected})
  });
}

  _handleConnectivityChange = (isConnected:any) => {
    this.setState({isConnected})
  };

  header() {
    const subjectName = this.props.navigation.getParam('subjectName');
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 1
            }}>

            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.goBack()}>

              <Image
                source={require('../Assets/back_arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(15),
                  width: verticalScale(21)
                  
                }}
              />
            </Click>
          </View>

          <View
            style={{
              flex: 6,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
            singleLine
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              {subjectName}
            </TextComponent>
          </View>

          <View
            style={{
              flex: 2,
              justifyContent: 'flex-start',
              alignItems: 'flex-end'
            }}
          >
            <Click onPress={() => {}}>
              <Image
                source={require('../Assets/Arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(19),
                  width: verticalScale(19)
                }}
              />
            </Click>
          </View>
        </View>
      </Header>
    );
  }

  updateAnswer(questions:any,index:number,userSelectedAnswer:number){
    const tempArr = [...questions];
    tempArr[index].user_ans = userSelectedAnswer;
    console.log(tempArr);
    this.props.updateState('questions', tempArr);
  }

  renderAnswer = (
    questions: Array<any>,
    index: number,
    q: any,
    userSelectedAnswer: any
  ) => {
    return (
     <QuestionComponent 
     index={index}     
     q={q} 
     questions={questions}
     userSelectedAnswer={userSelectedAnswer} 
     updateAnswerHandler={()=>this.updateAnswer(questions,index,userSelectedAnswer)} />
    );
  };

  updateQuestionsState(q:any){
    this.setState({
      modalOpen: true,
      modalText: q.hint
    });
  }

  updateQuestions (){
    
  }

  questions = () => {
    const {
      questions,
      questionCount,
      currentQuestion,
      silentLoading
    } = this.props.state;

    const subjectName = this.props.navigation.getParam('subjectName');

    return (
      <Swiper
        style={{ height: Utility.getHeight(960 / 1334) }}
        dotColor={'transparent'}
        loop={false}
        loadMinimal
        //@ts-ignore
        loadMinimalSize={4}
        showsButtons={false}
        showsPagination={false}
        index={currentQuestion - 1}
        onIndexChanged={(index: number) => {
          this.props.updateState('currentQuestion', index + 1);
          if (questions.length < questionCount) {
            if (index + 1 >= questions.length - 25 && silentLoading === false) {
              const currentPage = Math.ceil(questions.length / 50);
              this.props.getQuestions(
                currentPage + 1,
                { subject: subjectName },
                questions
              );
            }
          }
        }}>
       
        {questions.map((q: any, index: number) => (
          <QuestionRenderer 
          key={index}
          index={index} 
          q={q}
          questions={questions}
          result={this.result}
          renderAnswer={this.renderAnswer}
          updateQuestionsStatehandler={()=>this.updateQuestionsState(q)}  />
        ))}
      </Swiper>
      
    );
  };

  result = (q: any) => {
    return (
      <View
        style={{
        height: Utility.getHeight(170 / 1334),
        padding: 15
        }}>

        {q.user_ans == '' ? (
          <Fragment />
        ) : (
          <View
            style={{
              height: Utility.getWidth(60 / 750),
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center'
            }}>

            <Image
              source={
                q.user_ans == q.true_ans
                  ? require('../Assets/correct.png')
                  : require('../Assets/incorrect.png')
              }
              style={{
                height: Utility.getWidth(60 / 750),
                width: Utility.getWidth(60 / 750)
              }}
              resizeMode={'contain'}
            />
            <TextComponent> </TextComponent>
            {q.user_ans == q.true_ans ? (
              <TextComponent
                fontFamily={Theme.font.bold}
                size={'h3'}
                style={{
                  color: Theme.color.success,
                  textAlignVertical: 'center',
                  marginTop: Platform.OS == 'ios' ? verticalScale(2) : 0
                }}
              >
                Correct. Nice work!
              </TextComponent>
            ) : (
              <TextComponent
                fontFamily={Theme.font.bold}
                size={'h3'}
                style={{
                  color: Theme.color.danger,
                  textAlignVertical: 'center',
                  marginTop: Platform.OS == 'ios' ? verticalScale(2) : 0
                }}
              >
                Incorrect. Try again!
              </TextComponent>
            )}
          </View>
        )}
      </View>
    );
  };

  render() {
    const { loading, questionCount, currentQuestion } = this.props.state;
    const {isConnected} = this.state
    const Palatino = Platform.select({
      ios: `Palatino-Linotype-Regular.ttf`,
      android: `file:///android_asset/fonts/Palatino-Linotype-Regular.ttf`
    });

    return (
      <Container
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
        style={{ backgroundColor: Theme.color.white }}>
          
        {this.header()}
        <View
          style={{
            height: Utility.getHeight(135 / 1334),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Theme.color.white
          }}
        >
          {questionCount ? (
            <TextComponent
              style={{ color: Theme.color.darkerShadow }}
              fontFamily={Theme.font.bold}
              size={'h3'}
            >
              Questions No. {currentQuestion} of {questionCount}
            </TextComponent>
          ) : (
            <Fragment />
          )}
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: Theme.color.white,
          }}
        >
          {isConnected ? 
          <View style={{flex:1}}>
            {loading ? <LoadingScreen /> : this.questions()}
          </View>
          :
            <NoInternetView />
          }
          
        </View>
        <Modal
          visible={this.state.modalOpen}
          transparent
          onRequestClose={() => this.setState({ modalOpen: false })}
        >
          <View style={{ flex: 1 }}>
            <Click
              style={{ flex: 1, backgroundColor: Theme.color.overlay }}
              onPress={() => this.setState({ modalOpen: false })}
            />
            <View
              style={{
                flex: 3,
                paddingHorizontal: 15,
                backgroundColor: Theme.color.overlay
              }}
            >
              <View
                style={{
                  flex: 1,
                  backgroundColor: Theme.color.white,
                  borderRadius: 10,
                  overflow: 'hidden'
                }}
              >
                <View
                  style={{
                    height: Utility.getHeight(65 / 1334),
                    paddingHorizontal: 15,
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    flexDirection: 'row'
                  }}
                >
                  <Click
                    style={{ flex: 0 }}
                    onPress={() => this.setState({ modalOpen: false })}
                  >
                    <Image
                      source={require('../Assets/close.png')}
                      style={{
                        height: verticalScale(15),
                        width: verticalScale(15)
                      }}
                    />
                  </Click>
                </View>
                <Body
                  style={{ padding: 15, backgroundColor: Theme.color.white }}
                >
                  <WebView
                    source={{
                      html: `
                      <!DOCTYPE html>
                      <html>
                      <head>
                      <title>Page Title</title>
                      <meta name="viewport" content="width=device-width, initial-scale=1">

                      <style>
                      p{
                        font-family: 'Palatino Linotype';
                      }
                      body {
                        margin: 0,
                      }
                      @font-face {
                        font-family: 'Palatino Linotype';
                        src: url(${
                          Platform.OS === 'ios'
                            ? 'Palatino-Linotype-Regular.ttf'
                            : 'file:///android_asset/fonts/Palatino-Linotype-Regular.ttf'
                        }) format('woff2');
                        }
                      
                      </style>
                      </head>
                      <body>
                      <div><p style="font-size:${verticalScale(
                        16
                      )}px;text-align:center;color:${Theme.color.darkerShadow};
                      "
                      >${this.state.modalText}</p></div>
                      </body>
                      </html>
                  `
                    }}
                    allowsInlineMediaPlayback
                    mixedContentMode={'always'}
                    allowFileAccess
                    style={{ backgroundColor: 'transparent' }}
                  />
                  <View style={{ height: Utility.getHeight(45 / 1334) }} />
                  <View
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                  >
                    <View
                      style={{
                        height: Utility.getHeight(85 / 1334),
                        width: Utility.getWidth(155 / 750)
                      }}
                    >
                      <Card
                        style={{
                          borderRadius: 5,
                          // overflow: 'hidden',
                          height: Utility.getHeight(85 / 1334),
                          width: Utility.getWidth(155 / 750),
                          backgroundColor: Theme.color.primary
                        }}
                      >
                        <Click
                          onPress={() => {
                            this.setState({ modalOpen: false });
                          }}
                          style={{
                            flex: 1,
                            backgroundColor: Theme.color.primary,
                            justifyContent: 'center',
                            alignItems: 'center'
                          }}
                        >
                          <TextComponent
                            fontFamily={Theme.font.bold}
                            style={{ color: Theme.color.light }}
                            iosMargin
                          >
                            OK
                          </TextComponent>
                        </Click>
                      </Card>
                    </View>
                  </View>
                  <View style={{ height: Utility.getHeight(50 / 1334) }} />
                </Body>
              </View>
            </View>
            <Click
              style={{ flex: 1, backgroundColor: Theme.color.overlay }}
              onPress={() => this.setState({ modalOpen: false })}
            />
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.questionReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  updateState,
  getQuestions
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(QuestionScreen)
);
