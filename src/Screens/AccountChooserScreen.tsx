import React, { Component } from 'react';
import { Container, Click } from '../RNMUI';
import { connect } from 'react-redux';
import { NavigationScreenProp } from 'react-navigation';
import Theme from '../Utilities/Theme';
import { AccountType } from '../Components';

interface AccountChooserScreenInterface {
  navigation: NavigationScreenProp<any>;
}

class AccountChooserScreen extends Component<AccountChooserScreenInterface> {
  render() {
    return (
      <Container style={{ backgroundColor: Theme.color.primary }}>
        <Click
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Theme.color.light
          }}
          onPress={() => {
            this.props.navigation.navigate('Register', { accountType: 1 });
          }}
        >
          <AccountType
            type={1}
            imageBackground={Theme.color.shadow}
            accountColor={Theme.color.dark}
            descColor={Theme.color.lightShadow}
          />
        </Click>
        <Click
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Theme.color.primary
          }}
          onPress={() => {
            this.props.navigation.navigate('Register', {
              accountType: 2
            });
          }}
        >
          <AccountType
            type={2}
            imageBackground={Theme.color.shadow}
            accountColor={Theme.color.white}
            descColor={Theme.color.light}
          />
        </Click>
      </Container>
    );
  }
}

const mapStateToProps = null;

const mapDispatchToProps = null;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountChooserScreen);
