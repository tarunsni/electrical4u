import React, { Component } from 'react';
import { Container, Body, Header, Card, Click } from '../RNMUI';
import { connect } from 'react-redux';
import Theme from '../Utilities/Theme';
import { View, Image, Platform } from 'react-native';
import { TextComponent } from '../Components';
import { verticalScale } from 'react-native-size-matters';
import Utility from '../Utilities/Utility';
import { NavigationScreenProp } from 'react-navigation';
import { updateAppState } from '../Redux/ActionCreators/appAction';

interface ProfileScreenInterface {
  navigation: NavigationScreenProp<any>;
  updateAppState: Function;
  appState: any;
}

class ProfileScreen extends Component<ProfileScreenInterface> {
  state = {
    list: []
  };

  componentDidMount() {
    const { appState } = this.props;
    const { userData } = appState;
    this.setState({
      list: [
        {
          label: 'Name',
          value: userData ? userData.userName : ''
        },
        {
          label: 'Email',
          value: userData ? userData.email : ''
        }
        // {
        //   label: 'Phone Number',
        //   value: userData ? userData.phoneNumber : ''
        // },
        // {
        //   label: 'Gender',
        //   value: userData ? (userData.gender == '0' ? 'Male' : 'Female') : ''
        // },
        // {
        //   label: 'Date of Birth',
        //   value: userData ? userData.dob : ''
        // }
      ]
    });
  }

  header() {
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.goBack()}
            >
              <Image
                source={require('../Assets/back_arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(15),
                  width: verticalScale(21)
                }}
              />
            </Click>
          </View>

          <View
            style={{
              flex: 8,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              Profile
            </TextComponent>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.navigate('EditProfile')}
            >
              <Image
                source={require('../Assets/edit.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1
                }}
              />
            </Click>
          </View>
        </View>
      </Header>
    );
  }

  cardListItem = (label: any, value: any, index: number) => {
    return (
      <View key={index}>
        <View
          style={{
            minHeight: Utility.getHeight(100 / 1334),
            backgroundColor: Theme.color.white,
            flexDirection: 'row',
            borderBottomColor: Theme.color.shadow,
            borderBottomWidth: index === this.state.list.length - 1 ? 0 : 0.5,
            paddingHorizontal: 20,
            marginTop: 0.5
          }}
        >
          <View
            style={{
              flex: 4,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              fontFamily={Theme.font.regular}
              style={{ color: Theme.color.primary }}
              iosMargin
              size={'h5'}
            >
              {label}
            </TextComponent>
          </View>

          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <TextComponent
              fontFamily={Theme.font.regular}
              style={{ color: Theme.color.primary }}
              iosMargin
              size={'h5'}
            >
              -
            </TextComponent>
          </View>

          <View
            style={{
              flex: 5,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              fontFamily={Theme.font.regular}
              style={{ color: Theme.color.darkShadow }}
              iosMargin
              size={'h5'}
            >
              {value}
            </TextComponent>
          </View>
        </View>
      </View>
    );
  };

  render() {
    const { appState } = this.props;
    const { userData } = appState;
    return (
      <Container
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
      >
        {this.header()}
        <Body>
          <View style={{ paddingHorizontal: Utility.getWidth(25 / 750) }}>
            <View
              style={{
                height: Utility.getHeight(275 / 1334),
                flexDirection: 'row',
                paddingHorizontal: 20
              }}
            >
              <View
                style={{
                  flex: 7,
                  justifyContent: 'center',
                  alignItems: 'flex-start'
                }}
              >
                <TextComponent
                  size={'h3'}
                  fontFamily={Theme.font.bold}
                  iosMargin
                >
                  {userData ? userData.userName : ''}
                </TextComponent>
                <TextComponent
                  size={'h5'}
                  style={{ color: Theme.color.darkerShadow }}
                  fontFamily={Theme.font.regular}
                >
                  {userData ? userData.email : ''}
                </TextComponent>
              </View>
              <View
                style={{
                  flex: 3,
                  justifyContent: 'center',
                  alignItems: 'flex-end'
                }}
              >
                <Image
                  source={require('../Assets/user_placeholder.png')}
                  style={{
                    height: Utility.getHeight(150 / 1334),
                    width: Utility.getHeight(150 / 1334),
                    borderRadius: Utility.getHeight(150 / 1334) / 2
                  }}
                  resizeMode={'contain'}
                />
              </View>
            </View>
            <Card
              style={{
                borderRadius: 5,
                overflow: 'hidden'
              }}
              elevation={5}
            >
              {this.state.list.map((listItem: any, index: number) =>
                this.cardListItem(listItem.label, listItem.value, index)
              )}
            </Card>
          </View>
        </Body>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer
  };
};

const mapDispatchToProps = {
  updateAppState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);
