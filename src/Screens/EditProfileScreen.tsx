import React, { Component } from 'react';
import { Container, Body, Header, Card, Click } from '../RNMUI';
import { connect } from 'react-redux';
import Theme from '../Utilities/Theme';
import { View, Image, Platform, ActivityIndicator } from 'react-native';
import { TextComponent, EditProfileForm } from '../Components';
import { verticalScale } from 'react-native-size-matters';
import Utility from '../Utilities/Utility';
import { NavigationScreenProp } from 'react-navigation';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import { updateUserProfile } from '../Redux/ActionCreators/profileAction';

interface EditProfileScreenInterface {
  navigation: NavigationScreenProp<any>;
  updateAppState: Function;
  appState: any;
  state: any;
  updateUserProfile: Function;
}

class EditProfileScreen extends Component<EditProfileScreenInterface> {
  state = {
    userData: {}
  };

  componentDidMount() {
    this.initData();
  }

  initData = () => {
    const { appState } = this.props;
    const { userData } = appState;
    this.setState({
      userData
    });
  };

  updateProfile = () => {
    const { userData } = this.state;
    this.props.updateUserProfile(userData);
  };

  header() {
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.goBack()}
            >
              <Image
                source={require('../Assets/back_arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(15),
                  width: verticalScale(21)
                }}
              />
            </Click>
          </View>

          <View
            style={{
              flex: 8,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              Edit Profile
            </TextComponent>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          />
        </View>
      </Header>
    );
  }

  updateUserData = (name: string, value: string) => {
    this.setState({
      userData: {
        ...this.state.userData,
        [name]: value
      }
    });
  };

  render() {
    const { userData } = this.state;
    const { loading } = this.props.state;
    return (
      <Container
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
      >
        {this.header()}
        <Body>
          <View
            style={{
              height: Utility.getHeight(275 / 1334),
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Click
              onPress={() => {}}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
                position: 'relative'
              }}
            >
              <Image
                source={require('../Assets/user_placeholder.png')}
                style={{
                  height: verticalScale(64),
                  width: verticalScale(64),
                  borderRadius: verticalScale(64) / 2
                }}
                resizeMode={'contain'}
              />
              <View
                style={{
                  height: Utility.getHeight(50 / 1334),
                  width: Utility.getHeight(50 / 1334),
                  borderRadius: Utility.getHeight(50 / 1334) / 2,
                  backgroundColor: Theme.color.primary,
                  position: 'absolute',
                  top: Utility.getHeight(150 / 1334),
                  right: 0,
                  bottom: Utility.getHeight(75 / 1334),
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Image
                  source={require('../Assets/camera.png')}
                  style={{
                    flex: 1,
                    height: verticalScale(11),
                    width: verticalScale(13)
                  }}
                  resizeMode={'contain'}
                />
              </View>
            </Click>
          </View>
          <View style={{ paddingHorizontal: Utility.getWidth(25 / 750) }}>
            <Card
              style={{
                borderRadius: 5,
                overflow: 'hidden'
              }}
              elevation={5}
            >
              <EditProfileForm
                userData={userData}
                updateUserData={this.updateUserData}
              />
            </Card>
          </View>
          <View
            style={{
              height: Utility.getHeight(85 / 1334)
            }}
          />
          <View
            style={{
              paddingHorizontal: Utility.getWidth(25 / 750),
              flexDirection: 'row'
            }}
          >
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
              <Click
                onPress={this.initData}
                style={{
                  height: Utility.getHeight(80 / 1334),
                  width: Utility.getWidth(250 / 750),
                  backgroundColor: Theme.color.darkShadow,
                  borderRadius: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1
                }}
              >
                <TextComponent
                  fontFamily={Theme.font.bold}
                  style={{ color: Theme.color.white }}
                  iosMargin
                >
                  Cancel
                </TextComponent>
              </Click>
            </View>
            <View style={{ flex: 1, alignItems: 'flex-end' }}>
              <Click
                onPress={this.updateProfile}
                style={{
                  height: Utility.getHeight(80 / 1334),
                  width: Utility.getWidth(250 / 750),
                  backgroundColor: Theme.color.primary,
                  borderRadius: 30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1
                }}
                clickProps={{
                  disabled: loading
                }}
              >
                {loading ? (
                  <ActivityIndicator color={Theme.color.light} size={'large'} />
                ) : (
                  <TextComponent
                    fontFamily={Theme.font.bold}
                    style={{ color: Theme.color.white }}
                    iosMargin
                  >
                    Save
                  </TextComponent>
                )}
              </Click>
            </View>
          </View>
        </Body>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.profileReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  updateUserProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfileScreen);
