import React, { Component } from 'react';
import { Container, Body, Header, Click, Card } from '../RNMUI';
import { connect } from 'react-redux';
import { View, Image, Platform, ActivityIndicator } from 'react-native';
import { NavigationScreenProp, withNavigationFocus } from 'react-navigation';
import Utility from '../Utilities/Utility';
import {
  InputComponent,
  TextComponent,
  KeyboardSpaceriOS
} from '../Components';
import Theme from '../Utilities/Theme';
import { verticalScale, scale } from 'react-native-size-matters';
import {
  updateState,
  forgetPassword
} from '../Redux/ActionCreators/forgetPasswordAction';

interface ForgetPasswordScreenInterface {
  navigation: NavigationScreenProp<any>;
  state: any;
  updateState: Function;
  forgetPassword: Function;
  isFocused: boolean;
}

class ForgetPasswordScreen extends Component<ForgetPasswordScreenInterface> {
  componentDidUpdate(prevProps: ForgetPasswordScreenInterface) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.props.updateState('email', '');
    }
  }

  validateForgetPassword = () => {
    const { state, forgetPassword } = this.props;
    const { email } = state;
    if (!email.trim()) {
      return Utility.showErrorToast('Email is required');
    }
    forgetPassword({ email });
  };

  render() {
    const { state, updateState } = this.props;
    const { email, loading } = state;
    return (
      <Container>
        <Header
          type={'noBorder'}
          style={{
            height:
              Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
          }}
        >
          <View
            style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}
          >
            <View
              style={{
                flex: 1
              }}
            >
              <Click
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'flex-start'
                }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image
                  source={require('../Assets/header-back.png')}
                  resizeMode={'contain'}
                  style={{
                    height: verticalScale(15),
                    width: scale(21)
                  }}
                />
              </Click>
            </View>
            <View
              style={{
                flex: 6,
                justifyContent: 'center',
                alignItems: 'flex-start'
              }}
            >
              <TextComponent fontFamily={Theme.font.bold} iosMargin>
                Forgot Your Password
              </TextComponent>
            </View>
            <View style={{ flex: 1 }} />
          </View>
        </Header>
        <Body>
          <View
            style={{
              height: Utility.getHeight(370 / 1334),
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                height: Utility.getHeight(200 / 1334),
                width: Utility.getHeight(200 / 1334),
                backgroundColor: Theme.color.shadow,
                borderRadius: Utility.getHeight(200 / 1334) / 2,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={require('../Assets/vector-email.png')}
                style={{ flex: 1, height: verticalScale(36), width: scale(62) }}
                resizeMode={'contain'}
              />
            </View>
          </View>
          <View
            style={{
              height: Utility.getHeight(140 / 1334),
              paddingHorizontal: Utility.getWidth(100 / 750)
            }}
          >
            <TextComponent
              fontFamily={Theme.font.regular}
              size={'h5'}
              style={{
                color: Theme.color.darkerShadow,
                fontWeight: 'normal',
                textAlign: 'center'
              }}
            >
              Please enter your Email Address to receive a verification code.
            </TextComponent>
          </View>
          <View
            style={{
              height: Utility.getHeight(100 / 1334),
              paddingHorizontal: Utility.getWidth(42 / 750)
            }}
          >
            <InputComponent
              placeholder={'Email Address'}
              noBorder
              leftComponent={
                <Image
                  source={require('../Assets/email.png')}
                  style={{
                    height: Utility.getHeight(35 / 1334),
                    width: Utility.getHeight(35 / 1334)
                  }}
                  resizeMode={'contain'}
                />
              }
              value={email}
              inputProps={{
                autoCapitalize: 'none'
              }}
              onChangeText={(value: string) => updateState('email', value)}
            />
          </View>
          <View style={{ height: Utility.getHeight(96 / 1334) }} />
          <View
            style={{
              paddingHorizontal: Utility.getWidth(122 / 750)
            }}
          >
            <Card>
              <Click
                style={{
                  backgroundColor: Theme.color.primary,
                  height: Utility.getHeight(100 / 1334),
                  borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={this.validateForgetPassword}
                clickProps={{
                  disabled: loading
                }}
              >
                {loading ? (
                  <ActivityIndicator color={Theme.color.light} size={'large'} />
                ) : (
                  <TextComponent
                    style={{
                      textAlignVertical: 'center',
                      color: Theme.color.white,
                      fontWeight: Platform.OS == 'android' ? '200' : 'bold'
                    }}
                    fontFamily={Theme.font.bold}
                    iosMargin
                  >
                    Submit
                  </TextComponent>
                )}
              </Click>
            </Card>
          </View>
        </Body>
        <KeyboardSpaceriOS />
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    state: state.forgetPasswordReducer
  };
};

const mapDispatchToProps = {
  updateState,
  forgetPassword
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ForgetPasswordScreen)
);
