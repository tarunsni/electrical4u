import React, { Component } from 'react';
import { Container, Body, Header, Card, Click } from '../RNMUI';
import { connect } from 'react-redux';
import Theme from '../Utilities/Theme';
import { View, Image, Platform, NetInfo, ImageBackground, Alert } from 'react-native';
import { TextComponent, InputComponent } from '../Components';
import { verticalScale } from 'react-native-size-matters';
import Utility from '../Utilities/Utility';
import { NavigationScreenProp } from 'react-navigation';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import {
  updateState,
  getSubjectList
} from '../Redux/ActionCreators/homeAction';
import HomeStateInterface from '../Interfaces/Shared/HomeStateInterface';
import LoadingScreen from '../Components/Shared/LoadingScreen';
import NoInternetView from '../Components/Shared/NoInternetView';

interface HomeScreenInterface {
  navigation: NavigationScreenProp<any>;
  getSubjectList: Function;
  state: HomeStateInterface;
}

class HomeScreen extends Component<HomeScreenInterface> {
  state = {
    subjectList: [],
    search: '',
    first_time: false,
    isConnected:true,
  };

  componentDidMount() {
    this.props.getSubjectList();
  }

  componentWillMount() {
    this.addListener()
    this.search('');
  }


  componentWillUnmount(){
    this.removeConnectionListener()
  }

  componentDidUpdate() {
    let arr = this.props.state.subjectList.sort( function( a, b ) {
      return a.subject < b.subject ? -1 : a.subject > b.subject ? 1 : 0;
  })

    if (
      this.props.state.subjectList.length > 0 &&
      this.state.subjectList.length == 0 &&
      this.state.first_time == false
    ) {
      this.setState({
        subjectList: arr,
        first_time: true
      });
    }
  }

  search = (text: string) => {
    
    const tempArr = [...this.props.state.subjectList.sort()]
    const filteredArray = tempArr.filter(
      t => t.subject.toLowerCase().indexOf(text.toLowerCase()) >= 0
    );
    
    this.setState({ search: text, subjectList: filteredArray });
  };

  header() {
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 8,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              MCQ SUBJECTS
            </TextComponent>
          </View>
          <View
            style={{
              flex: 2.4,
              justifyContent: 'flex-start',
              alignItems: 'flex-end'
            }}
          >
            <Click onPress={() => {}}>
              <Image
                source={require('../Assets/Arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(19),
                  width: verticalScale(19)
                }}
              />
            </Click>
          </View>
          <View
            style={{
              flex: 1.6,
              justifyContent: 'flex-start',
              alignItems: 'flex-end'
            }}
          >
            <Click onPress={() => this.props.navigation.navigate('Settings')}>
              <Image
                source={require('../Assets/settings.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(19),
                  width: verticalScale(19)
                }}
              />
            </Click>
          </View>
        </View>
      </Header>
    );
  }

  getImage(subject: string) {
    switch (subject) {
      case 'Electric Circuits':
        return require('../Assets/subjects/electric_circuits.png');
      case 'Electromagnetic Fields':
        return require('../Assets/subjects/electromagnetic_fields.png');
      case 'Electrical and Electronics Measurements':
        return require('../Assets/subjects/electrical_measurements.png');
      case 'Analog Electronics':
        return require('../Assets/subjects/analog_electronics.png');
      case 'Electrical Machines':
        return require('../Assets/subjects/electrical-machines-mcqs.jpg');
      case 'Control Systems':
        return require('../Assets/subjects/control_systems.png');
      case 'Digital Electronics':
        return require('../Assets/subjects/digital_electronics.png');
      case 'Electrical Drives':
        return require('../Assets/subjects/electrical_drives.png');
      case 'Analog Digital Electronics':
        return require('../Assets/subjects/analog&digital.png');
      case 'Power Electronics':
        return require('../Assets/subjects/power_electronics.png');
      case 'Power Systems':
        return require('../Assets/subjects/power-systems-mcqs.jpg');
      case 'Engineering Materials':
        return require('../Assets/subjects/engineering_materials.png');
      case 'Illumination':
        return require('../Assets/subjects/illumination.png');
      case 'Engineering Mathematics':
        return require('../Assets/subjects/engineering-mathematics-mcqs.jpg');
      case 'Microprocessor':
        return require('../Assets/subjects/microprocessor-mcqs.jpg');
      case 'Heating and Welding':
        return require('../Assets/subjects/heating_welding.png');
      default:
        return require('../Assets/subjects/electric_circuits.png');
    }
  }

  getSubject = (subjectData: any) => {
    return (
      <Click
        style={{
          height: Utility.getHeight(314 / 1334),
          backgroundColor: 'black',
          borderRadius: 10,
          overflow: 'hidden'
        }}
        onPress={() => {
          this.props.navigation.navigate('Question', {
            subjectName: subjectData.subject
          });
        }}
      >
        <ImageBackground
          source={this.getImage(subjectData.subject)}
          style={{
            flex: 1
          }}
          resizeMode={'cover'}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'center',
              padding: 10,
              backgroundColor: 'rgba(0,0,0,0.4)'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light,
                textAlign: 'center'
              }}
              fontFamily={Theme.font.bold}
              size={'h4'}
              disableScaling
            >
              {subjectData.subject}
            </TextComponent>
          </View>
        </ImageBackground>
      </Click>
    );
  };


  removeConnectionListener(){
    NetInfo.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange,
    );
  }

  addListener(){
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange
  ); 
  NetInfo.isConnected.fetch().then((isConnected:any) => {
    this.setState({isConnected})
  });
}

  _handleConnectivityChange = (isConnected:any) => {
    this.setState({isConnected})
  };


  render() {
    const { loading } = this.props.state;
    const { search } = this.state;
    return (
      <Container
      
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
      >
        {this.header()}
        <View
          style={{
            height: Utility.getHeight(20 / 1334)
          }}
        />
        <View
          style={{
            paddingHorizontal: Utility.getWidth(25 / 750)
          }}
        >
          <Card
            style={{
              height: Utility.getHeight(75 / 1334),
              borderRadius: 5,
              overflow: 'hidden',
              borderWidth: 0.2,
              borderColor: Theme.color.lightShadow
            }}
            elevation={5}
          >
            <InputComponent
              placeholder={'Search...'}
              noRadius
              leftComponent={
                <Image
                  source={require('../Assets/search.png')}
                  style={{
                    height: verticalScale(15),
                    width: verticalScale(15)
                  }}
                  resizeMode={'contain'}
                />
              }
              inputProps={{
                //@ts-ignore
                height: Utility.getHeight(75 / 1334),
                textAlignVertical: 'center'
              }}
              value={search}
              onChangeText={this.search}
            />
          </Card>
          <View
            style={{
              height: Utility.getHeight(40 / 1334)
            }}
          />
        </View>
        {this.state.isConnected ? 
        <View style={{flex:1}}>
        {loading ? (
          <LoadingScreen />
        ) : (
          <Body>
            <View
              style={{
                flexWrap: 'wrap',
                flexDirection: 'row'
              }}
            >
              {this.state.subjectList.map((d: any, i: number) => (
                <View
                  key={i}
                  style={{
                    flexBasis: '50%',
                    maxWidth: '50%',
                    paddingLeft: Utility.getWidth(
                      ((i + 1) % 2 === 0 ? 22.5 : 25) / 750
                    ),
                    paddingRight: Utility.getWidth(
                      ((i + 1) % 2 === 0 ? 22.5 : 25) / 750
                    ),
                    paddingBottom: Utility.getHeight(30 / 1334)
                  }}
                >
                  {this.getSubject(d)}
                </View>
              ))}
            </View>
          </Body>
        )}
        </View>
        :
        <NoInternetView />
        }
        
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.homeReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  updateState,
  getSubjectList
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);
