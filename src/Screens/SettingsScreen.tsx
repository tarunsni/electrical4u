import React, { Component } from 'react';
import { Container, Body, Header, Card, Click } from '../RNMUI';
import { connect } from 'react-redux';
import Theme from '../Utilities/Theme';
import { View, Image, Platform, Alert } from 'react-native';
import { TextComponent } from '../Components';
import { verticalScale } from 'react-native-size-matters';
import Utility from '../Utilities/Utility';
import { NavigationScreenProp } from 'react-navigation';
import DataStore from '../Utilities/DataStore';
import { updateAppState } from '../Redux/ActionCreators/appAction';

interface SettingsScreenInterface {
  navigation: NavigationScreenProp<any>;
  updateAppState: Function;
  appState: any;
}

class SettingsScreen extends Component<SettingsScreenInterface> {
  logoutAlert = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
        {
          text: 'Yes',
          onPress: async () => {
            await DataStore.delete();
            await Utility.logout();
            this.props.updateAppState('loggedInStatus', 0);
            setTimeout(() => {
              this.props.updateAppState('loggedInStatus', 2);
            }, 150);
          }
        }
      ],
      { cancelable: false }
    );
  };

  list: Array<any> = [
    {
      leftImage: require('../Assets/edit_profile.png'),
      height: 15,
      width: 20,
      title: 'Edit Profile',
      onPress: () => {
        this.props.navigation.navigate('EditProfile');
      }
    },
    {
      leftImage: require('../Assets/Arrow_black.png'),
      height: 15,
      width: 20,
      title: 'Upgrade to Premium',
      onPress: () => {}
    },
    {
      leftImage: require('../Assets/privacy_policy.png'),
      height: 19,
      width: 15,
      title: 'Privacy Policy',
      onPress: () => {}
    },
    {
      leftImage: require('../Assets/phone.png'),
      height: 19,
      width: 19,
      title: 'Contact Us',
      onPress: () => {
        // this.props.navigation.navigate('ContactUs');
      }
    },
    {
      leftImage: require('../Assets/logout.png'),
      height: 18,
      width: 18,
      title: 'Logout',
      onPress: this.logoutAlert
    }
  ];

  header() {
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 1
            }}
          >
            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.goBack()}
            >
              <Image
                source={require('../Assets/back_arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(15),
                  width: verticalScale(21)
                }}
              />
            </Click>
          </View>

          <View
            style={{
              flex: 8,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              Settings
            </TextComponent>
          </View>
        </View>
      </Header>
    );
  }

  cardListItem = (
    leftImage: any,
    title: any,
    onPress: Function,
    height: number,
    width: number,
    index: number
  ) => {
    return (
      <Click onPress={onPress} key={index}>
        <View
          style={{
            height: Utility.getHeight(125 / 1334),
            backgroundColor: Theme.color.white,
            flexDirection: 'row',
            borderBottomColor: Theme.color.lightShadow,
            borderBottomWidth: (index + 1) % 2 ? 1 : 0.5
          }}
        >
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image
              source={leftImage}
              style={{
                flex: 1,
                height: verticalScale(height),
                width: verticalScale(width)
              }}
              resizeMode={'contain'}
            />
          </View>

          <View
            style={{
              flex: 7.1,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              fontFamily={Theme.font.regular}
              iosMargin
              size={'h3'}
            >
              {title}
            </TextComponent>
          </View>

          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image
              source={require('../Assets/next.png')}
              style={{ flex: 1 }}
              resizeMode={'contain'}
            />
          </View>
        </View>
      </Click>
    );
  };

  render() {
    const { appState } = this.props;
    const { userData } = appState;
    return (
      <Container
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
      >
        {this.header()}
        <Body>
          <View
            style={{
              height: Utility.getHeight(50 / 1334)
            }}
          />
          <View
            style={{
              height: Utility.getHeight(150 / 1334),
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Click
              onPress={() => this.props.navigation.navigate('Profile')}
              style={{
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={require('../Assets/user_placeholder.png')}
                style={{
                  height: verticalScale(64),
                  width: verticalScale(64),
                  borderRadius: verticalScale(64) / 2
                }}
                resizeMode={'contain'}
              />
            </Click>
          </View>
          <View
            style={{
              height: Utility.getHeight(20 / 1334)
            }}
          />
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <TextComponent size={'h2'} fontFamily={Theme.font.bold}>
              {userData ? userData.userName : ''}
            </TextComponent>
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <TextComponent
              size={'h4'}
              style={{ color: Theme.color.darkerShadow }}
              fontFamily={Theme.font.regular}
            >
              {userData ? userData.email : ''}
            </TextComponent>
          </View>
          <View
            style={{
              height: Utility.getHeight(80 / 1334)
            }}
          />

          <View style={{ paddingHorizontal: Utility.getWidth(25 / 750) }}>
            <Card
              style={{
                borderRadius: 5,
                overflow: 'hidden'
              }}
              elevation={5}
            >
              {this.list.map((listItem: any, index: number) =>
                this.cardListItem(
                  listItem.leftImage,
                  listItem.title,
                  listItem.onPress,
                  listItem.height,
                  listItem.width,
                  index
                )
              )}
            </Card>
          </View>
        </Body>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer
  };
};

const mapDispatchToProps = {
  updateAppState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsScreen);
