import React, { Component, Fragment } from 'react';
import { Container, Body, Card, Click, Spacer } from '../RNMUI';
import { connect } from 'react-redux';
import {
  View,
  Image,
  Platform,
  ActivityIndicator,
  Keyboard
} from 'react-native';
import Utility from '../Utilities/Utility';
import { NavigationScreenProp, withNavigationFocus } from 'react-navigation';
import {
  InputComponent,
  AccountType,
  TextComponent,
  KeyboardSpaceriOS
} from '../Components';
import Theme from '../Utilities/Theme';
import { verticalScale } from 'react-native-size-matters';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import {
  updateState,
  registerUser
} from '../Redux/ActionCreators/registerAction';
import RegisterStateInterface from '../Interfaces/Shared/RegisterStateInterface';

interface RegisterScreenInterface {
  navigation: NavigationScreenProp<any>;
  appState: any;
  state: RegisterStateInterface;
  updateState: Function;
  registerUser: Function;
  isFocused: boolean;
}

class RegisterScreen extends Component<RegisterScreenInterface> {
  componentWillMount() {
    const accountType = this.props.navigation.getParam('accountType');
    this.props.updateState('accountType', accountType);
  }

  componentDidMount() {
    Utility.setNavigation(this.props.navigation);
    this.props.navigation.addListener('willBlur', () => {
      Keyboard.dismiss();
    });
    ['name', 'email', 'password', 'cnfPassword'].forEach(d => {
      this.props.updateState(d, '');
    });
  }

  componentDidUpdate(prevProps: RegisterScreenInterface) {
    if (prevProps.isFocused !== this.props.isFocused) {
      ['name', 'email', 'password', 'cnfPassword'].forEach(d => {
        this.props.updateState(d, '');
      });
    }
  }

  accountType(accountType: number) {
    return (
      <View
        style={{
          height: Utility.getHeight(445 / 1334, true),
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1
        }}
      >
        <AccountType
          type={accountType}
          imageBackground={Theme.color.shadow}
          accountColor={Theme.color.dark}
          descColor={Theme.color.lightShadow}
        />
      </View>
    );
  }

  validateUser = async () => {
    const { state, registerUser } = this.props;
    const { name, email, password, cnfPassword, accountType } = state;

    if (!name.trim()) {
      return Utility.showErrorToast('Name is required');
    }

    if (!email.trim()) {
      return Utility.showErrorToast('Email is required');
    }

    if (!password.trim()) {
      return Utility.showErrorToast('Password is required');
    }

    if (!cnfPassword.trim()) {
      return Utility.showErrorToast('Confirm password is required');
    }

    if (cnfPassword !== password) {
      return Utility.showErrorToast('Password do not match');
    }

    const registerBody = {
      userName: name,
      email: email,
      password: password,
      accountType: accountType === 1 ? 'Free' : 'Premium',
      deviceToken: 'sdfadfa',
      deviceType: Platform.OS === 'ios' ? '1' : '2'
    };

    try {
      this.props.updateState('loading', true);
      await Utility.registerWithAuth0Realm({
        password: password,
        email: email
      });
      const responseAuth0 = await Utility.loginWithAuth0Realm({
        password: password,
        username: email
      });
      console.log(responseAuth0);
      registerUser(registerBody, responseAuth0, true);
    } catch (error) {
      if (error.message) {
        Utility.showErrorToast(error.message);
      } else {
        Utility.showErrorToast('Failed to connect with server.');
      }
      this.props.updateState('loading', false);
    }
  };

  registerForm = () => {
    const { state, updateState } = this.props;
    const { name, email, password, cnfPassword, loading } = state;
    return (
      <View
        style={{
          height: Utility.getHeight(516 / 1334),
          paddingHorizontal: Utility.getWidth(42 / 750)
        }}
      >
        <Card
          style={{
            flex: 1,
            backgroundColor: '#ffffff',
            borderRadius: 5,
            overflow: 'hidden'
          }}
        >
          <InputComponent
            placeholder={'Name'}
            noRadius
            leftComponent={
              <Image
                source={require('../Assets/user.png')}
                style={{
                  height: Utility.getHeight(35 / 1334),
                  width: Utility.getHeight(35 / 1334)
                }}
                resizeMode={'contain'}
              />
            }
            value={name}
            onChangeText={(value: string) => updateState('name', value)}
          />
          <InputComponent
            placeholder={'Email Address'}
            noRadius
            inputProps={{
              autoCapitalize: 'none',
              keyboardType: 'email-address'
            }}
            leftComponent={
              <Image
                source={require('../Assets/email.png')}
                style={{
                  height: Utility.getHeight(35 / 1334),
                  width: Utility.getHeight(35 / 1334)
                }}
                resizeMode={'contain'}
              />
            }
            value={email}
            onChangeText={(value: string) => updateState('email', value)}
          />
          <InputComponent
            placeholder={'Password'}
            noRadius
            leftComponent={
              <Image
                source={require('../Assets/lock.png')}
                style={{
                  height: Utility.getHeight(35 / 1334),
                  width: Utility.getHeight(35 / 1334)
                }}
                resizeMode={'contain'}
              />
            }
            inputProps={{ secureTextEntry: true }}
            value={password}
            onChangeText={(value: string) => updateState('password', value)}
          />
          <InputComponent
            placeholder={'Confirm Password'}
            noBorder
            noRadius
            leftComponent={
              <Image
                source={require('../Assets/lock.png')}
                style={{
                  height: Utility.getHeight(35 / 1334),
                  width: Utility.getHeight(35 / 1334)
                }}
                resizeMode={'contain'}
              />
            }
            inputProps={{ secureTextEntry: true }}
            value={cnfPassword}
            onChangeText={(value: string) => updateState('cnfPassword', value)}
          />
          <Click
            style={{
              height: Utility.getHeight(104 / 1334),
              backgroundColor: Theme.color.primary,
              justifyContent: 'center',
              alignItems: 'center'
            }}
            onPress={this.validateUser}
            clickProps={{
              disabled: loading
            }}
          >
            {loading ? (
              <ActivityIndicator color={Theme.color.light} size={'large'} />
            ) : (
              <TextComponent
                style={{
                  color: Theme.color.white,
                  fontWeight: Platform.OS == 'android' ? '200' : 'bold'
                }}
                iosMargin
                fontFamily={Theme.font.bold}
              >
                Register
              </TextComponent>
            )}
          </Click>
        </Card>
      </View>
    );
  };

  render() {
    const { state } = this.props;
    const { accountType } = state;
    return (
      <Container>
        <Body>
          {this.accountType(accountType)}
          {this.registerForm()}
          <View
            style={{
              height: Utility.getHeight(45 / 1334)
            }}
          />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Click
              style={{
                paddingHorizontal: Utility.getWidth(65 / 750),
                backgroundColor: Theme.color.primary,
                height: Utility.getHeight(72 / 1334),
                justifyContent: 'center',
                borderRadius: 25
              }}
              onPress={() =>
                this.props.updateState('accountType', accountType == 1 ? 2 : 1)
              }
            >
              <TextComponent
                style={{
                  color: '#ffffff',
                  fontFamily: Theme.font.bold,
                  marginTop: Platform.OS === 'ios' ? verticalScale(5) : 0
                }}
                iosMargin
                size={'h4'}
              >
                {accountType == 1 ? 'Premium Account' : 'Free Account'}
              </TextComponent>
            </Click>
          </View>
          <View
            style={{
              height: Utility.getHeight(45 / 1334)
            }}
          />
        </Body>
        <KeyboardSpaceriOS />
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.registerReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  updateState,
  registerUser
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RegisterScreen)
);
