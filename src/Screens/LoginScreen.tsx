import React, { Component, Fragment } from 'react';
import { Container, Body, Click, Card } from '../RNMUI';
import { connect } from 'react-redux';
import {
  View,
  Image,
  Platform,
  ActivityIndicator,
  Keyboard
} from 'react-native';
import Utility from '../Utilities/Utility';
import {
  TextComponent,
  KeyboardSpaceriOS,
  InputComponent,
  Checkbox
} from '../Components';
import Theme from '../Utilities/Theme';
import { NavigationScreenProp, withNavigationFocus } from 'react-navigation';
import { verticalScale, scale } from 'react-native-size-matters';
import { updateState, loginUser } from '../Redux/ActionCreators/loginAction';
import { updateAppState } from '../Redux/ActionCreators/appAction';
import LoginStateInterface from '../Interfaces/Shared/LoginStateInterface';

interface LoginScreenInterface {
  navigation: NavigationScreenProp<any>;
  state: LoginStateInterface;
  updateState: Function;
  loginUser: Function;
  updateAppState: Function;
  isFocused: boolean;
}

class LoginScreen extends Component<LoginScreenInterface> {
  componentDidMount() {
    Utility.setNavigation(this.props.navigation);
    this.props.navigation.addListener('willBlur', () => {
      Keyboard.dismiss();
    });
    ['email', 'password'].forEach(d => {
      this.props.updateState(d, '');
    });
  }

  componentDidUpdate(prevProps: LoginScreenInterface) {
    if (prevProps.isFocused !== this.props.isFocused) {
      ['email', 'password'].forEach(d => {
        this.props.updateState(d, '');
      });
      this.props.updateState('rememberMe', true);
    }
  }

  logo() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          height: Utility.getHeight(380 / 1334)
        }}
      >
        <Image
          source={require('../Assets/login_logo.png')}
          style={{
            flex: 1,
            height: verticalScale(102),
            width: scale(235)
          }}
          resizeMode={'contain'}
        />
      </View>
    );
  }

  loginForm = () => {
    const { state, updateState } = this.props;
    const { email, password, loading, rememberMe } = state;
    return (
      <Fragment>
        <View
          style={{
            height: Utility.getHeight(100 / 1334),
            paddingHorizontal: Utility.getWidth(42 / 750)
          }}
        >
          <InputComponent
            placeholder={'Email Address'}
            inputProps={{
              keyboardType: 'email-address',
              autoCapitalize: 'none'
            }}
            noBorder
            leftComponent={
              <Image
                source={require('../Assets/email.png')}
                style={{
                  height: verticalScale(11),
                  width: verticalScale(15)
                }}
                resizeMode={'contain'}
              />
            }
            value={email}
            onChangeText={(value: string) => updateState('email', value)}
          />
        </View>
        <View style={{ height: Utility.getHeight(35 / 1334) }} />
        <View
          style={{
            height: Utility.getHeight(100 / 1334),
            paddingHorizontal: Utility.getWidth(42 / 750)
          }}
        >
          <InputComponent
            inputProps={{ secureTextEntry: true }}
            placeholder={'Password'}
            noBorder
            leftComponent={
              <Image
                source={require('../Assets/lock.png')}
                style={{
                  height: verticalScale(15),
                  width: verticalScale(11)
                }}
                resizeMode={'contain'}
              />
            }
            value={password}
            onChangeText={(value: string) => updateState('password', value)}
          />
        </View>
        <View style={{ height: Utility.getHeight(35 / 1334) }} />

        <View
          style={{
            height: Utility.getHeight(38.8 / 1334),
            paddingHorizontal: Utility.getWidth(42 / 750),
            flexDirection: 'row'
          }}
        >
          <Checkbox
            checked={rememberMe}
            onPress={() => updateState('rememberMe', !rememberMe)}
          />
          <View
            style={{
              flex: 9,
              justifyContent: 'center'
            }}
          >
            <TextComponent
              size={'h4'}
              style={{
                color: Theme.color.lightShadow,
                fontWeight: '200'
              }}
              fontFamily={Theme.font.regular}
            >
              Keep me signed in
            </TextComponent>
          </View>
        </View>

        <View style={{ height: Utility.getHeight(90 / 1334) }} />

        <View
          style={{
            paddingHorizontal: Utility.getWidth(122 / 750)
          }}
        >
          <Card>
            <Click
              style={{
                backgroundColor: Theme.color.primary,
                height: Utility.getHeight(100 / 1334),
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center'
              }}
              onPress={this.validateUser}
              clickProps={{
                disabled: loading
              }}
            >
              {loading ? (
                <ActivityIndicator color={Theme.color.light} size={'large'} />
              ) : (
                <TextComponent
                  style={{
                    color: Theme.color.white,
                    fontWeight: Platform.OS == 'android' ? '200' : 'bold'
                  }}
                  iosMargin
                  fontFamily={Theme.font.bold}
                >
                  Login
                </TextComponent>
              )}
            </Click>
          </Card>
        </View>
      </Fragment>
    );
  };

  validateUser = async () => {
    const { state, loginUser } = this.props;
    const { email, password, rememberMe } = state;

    if (!email.trim()) {
      return Utility.showErrorToast('Email is required');
    }

    if (!password.trim()) {
      return Utility.showErrorToast('Password is required');
    }

    const loginBody = {
      email: email,
      password: password,
      deviceToken: 'sdfadfa',
      deviceType: Platform.OS === 'ios' ? '1' : '2'
    };

    try {
      this.props.updateState('loading', true);
      const responseAuth0 = await Utility.loginWithAuth0Realm({
        password: password,
        username: email
      });
      loginUser(loginBody, responseAuth0, rememberMe);
    } catch (error) {
      if (error.message) {
        Utility.showErrorToast(error.message);
      } else {
        Utility.showErrorToast('Failed to connect with server.');
      }
      this.props.updateState('loading', false);
    }
  };

  footer() {
    return (
      <Fragment>
        <View style={{ height: Utility.getHeight(80 / 1334) }} />
        <Click
          style={{
            height: Utility.getHeight(50 / 1334)
          }}
          onPress={() => this.props.navigation.navigate('Forget')}
        >
          <TextComponent
            size={'h4'}
            style={{
              color: Theme.color.primary,
              textAlign: 'center'
            }}
            fontFamily={Theme.font.bold}
          >
            Forgot your password?
          </TextComponent>
        </Click>
        <View style={{ height: Utility.getHeight(100 / 1334) }} />
      </Fragment>
    );
  }

  render() {
    return (
      <Container>
        <Body>
          {this.logo()}
          {this.loginForm()}
          {this.footer()}
        </Body>
        <KeyboardSpaceriOS />
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    appState: state.appReducer,
    state: state.loginReducer
  };
};

const mapDispatchToProps = {
  updateAppState,
  updateState,
  loginUser
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(LoginScreen)
);
