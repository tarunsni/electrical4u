import React, { Component } from 'react';
import { Container, Body, Title, Header } from '../RNMUI';
import { connect } from 'react-redux';

class InitialScreen extends Component {
  render() {
    return (
      <Container>
        <Header />
        <Body style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Title>Initial Screen</Title>
        </Body>
      </Container>
    );
  }
}

const mapStateToProps = null;

const mapDispatchToProps = null;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InitialScreen);
