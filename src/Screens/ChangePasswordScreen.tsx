import React, { Component } from 'react';
import { Container, Body, Header, Click, Card } from '../RNMUI';
import { connect } from 'react-redux';
import { View, Image, Platform, ActivityIndicator } from 'react-native';
import { NavigationScreenProp, withNavigationFocus } from 'react-navigation';
import Utility from '../Utilities/Utility';
import {
  InputComponent,
  TextComponent,
  KeyboardSpaceriOS
} from '../Components';
import Theme from '../Utilities/Theme';
import { verticalScale, scale } from 'react-native-size-matters';
import {
  updateState,
  forgetPassword
} from '../Redux/ActionCreators/forgetPasswordAction';

interface ChangePasswordScreenInterface {
  navigation: NavigationScreenProp<any>;
  state: any;
  updateState: Function;
  forgetPassword: Function;
  isFocused: boolean;
}

class ChangePasswordScreen extends Component<ChangePasswordScreenInterface> {
  componentDidUpdate(prevProps: ChangePasswordScreenInterface) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.props.updateState('email', '');
    }
  }

  validateForgetPassword = () => {
    const { state, forgetPassword } = this.props;
    const { email } = state;
    if (!email.trim()) {
      return Utility.showErrorToast('Email is required');
    }
    forgetPassword({ email });
  };

  header = () => {
    return (
      <Header
        type={'noBorder'}
        style={{
          backgroundColor: Theme.color.primary,
          height: Platform.OS === 'ios' ? verticalScale(56) : verticalScale(65)
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 15 }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Click
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.goBack()}
            >
              <Image
                source={require('../Assets/back_arrow_white.png')}
                resizeMode={'contain'}
                style={{
                  flex: 1,
                  height: verticalScale(15),
                  width: verticalScale(21)
                }}
              />
            </Click>
          </View>

          <View
            style={{
              flex: 8,
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              style={{
                color: Theme.color.light
              }}
              fontFamily={Theme.font.bold}
              iosMargin
            >
              Change Password
            </TextComponent>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          />
        </View>
      </Header>
    );
  };

  render() {
    const { state, updateState } = this.props;
    const { email, loading } = state;
    return (
      <Container
        statusBarBackgroundColor={Theme.color.primary}
        statusBarStyle={'light-content'}
      >
        {this.header()}
        <Body>
          <View
            style={{
              height: Utility.getHeight(370 / 1334),
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                height: Utility.getHeight(200 / 1334),
                width: Utility.getHeight(200 / 1334),
                backgroundColor: Theme.color.shadow,
                borderRadius: Utility.getHeight(200 / 1334) / 2,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={require('../Assets/lock_password.png')}
                style={{ flex: 1, height: verticalScale(36), width: scale(52) }}
                resizeMode={'contain'}
              />
            </View>
          </View>
          <View
            style={{
              height: Utility.getHeight(140 / 1334),
              paddingHorizontal: Utility.getWidth(100 / 750)
            }}
          >
            <TextComponent
              fontFamily={Theme.font.regular}
              size={'h5'}
              style={{
                color: Theme.color.darkerShadow,
                fontWeight: 'normal',
                textAlign: 'center'
              }}
            >
              Please enter your new password below.
            </TextComponent>
          </View>
          <View
            style={{
              height: Utility.getHeight(100 / 1334),
              paddingHorizontal: Utility.getWidth(42 / 750)
            }}
          >
            <InputComponent
              placeholder={'New Password'}
              noBorder
              leftComponent={
                <Image
                  source={require('../Assets/lock.png')}
                  style={{
                    height: verticalScale(15),
                    width: verticalScale(11)
                  }}
                  resizeMode={'contain'}
                />
              }
              value={email}
              inputProps={{
                autoCapitalize: 'none'
              }}
              onChangeText={(value: string) => updateState('email', value)}
            />
          </View>
          <View style={{ height: Utility.getHeight(50 / 1334) }} />

          <View
            style={{
              height: Utility.getHeight(100 / 1334),
              paddingHorizontal: Utility.getWidth(42 / 750)
            }}
          >
            <InputComponent
              placeholder={'Confirm Password'}
              noBorder
              leftComponent={
                <Image
                  source={require('../Assets/lock.png')}
                  style={{
                    height: verticalScale(15),
                    width: verticalScale(11)
                  }}
                  resizeMode={'contain'}
                />
              }
              value={email}
              inputProps={{
                autoCapitalize: 'none'
              }}
              onChangeText={(value: string) => updateState('email', value)}
            />
          </View>

          <View style={{ height: Utility.getHeight(76 / 1334) }} />
          <View
            style={{
              paddingHorizontal: Utility.getWidth(122 / 750)
            }}
          >
            <Card>
              <Click
                style={{
                  backgroundColor: Theme.color.primary,
                  height: Utility.getHeight(100 / 1334),
                  borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={this.validateForgetPassword}
                clickProps={{
                  disabled: loading
                }}
              >
                {loading ? (
                  <ActivityIndicator color={Theme.color.light} size={'large'} />
                ) : (
                  <TextComponent
                    style={{
                      textAlignVertical: 'center',
                      color: Theme.color.white,
                      fontWeight: Platform.OS == 'android' ? '200' : 'bold'
                    }}
                    fontFamily={Theme.font.bold}
                    iosMargin
                  >
                    Change Password
                  </TextComponent>
                )}
              </Click>
            </Card>
          </View>
        </Body>
        <KeyboardSpaceriOS />
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    state: state.forgetPasswordReducer
  };
};

const mapDispatchToProps = {
  updateState,
  forgetPassword
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ChangePasswordScreen)
);
