import { UPDATE_QUESTION_STATE } from '../Types';
import HttpClient from '../../Utilities/HttpClient';
import Utility from '../../Utilities/Utility';

export const updateState = (name: string, value: any) => {
  return {
    type: UPDATE_QUESTION_STATE,
    payload: {
      name,
      value
    }
  };
};

export const getQuestions = (
  pageNo: number,
  body: any,    
  previousQuestions: Array<any> = []
) => async (dispatch: any) => {
  try {
    console.log('pageNo', pageNo, body);
    if (previousQuestions.length == 0) {
      dispatch(updateState('loading', true));
    } else {
      dispatch(updateState('silentLoading', true));
    }
    const responseData = await HttpClient.getQuestions(pageNo, body);
    console.log('ResponseData', responseData);
    dispatch(updateState('loading', false));
    dispatch(updateState('silentLoading', false));
    let questions = [
      ...previousQuestions,
      ...responseData.data.data.questions
    ];
    questions.forEach(q => (q.user_ans = ''));
    questions = questions.map(q => {
      return {
        ...q, hint: q.hint.replace(/&quot;/g, `"`)
        .replace(
          /src="/g,
          `src="${HttpClient.base_url.substr(
            0,
            HttpClient.base_url.length - 1
          )}`
        )
        .replace(
          /href=/g,
          `href=${HttpClient.base_url.substr(
            0,
            HttpClient.base_url.length - 1
          )}`
        ),
        que_desc: q.que_desc.replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, `"`)
        .replace(
          /src="/g,
          `src="${HttpClient.base_url.substr(
            0,
            HttpClient.base_url.length - 1
          )}`
        )
        .replace(
          /href=/g,
          `href=${HttpClient.base_url.substr(
            0,
            HttpClient.base_url.length - 1
          )}`
        )
      }
    })
    // console.log({questions})
    dispatch(updateState('questions', questions || []));
    dispatch(updateState('questionCount', responseData.data.data.counts));
  } catch (error) {
    Utility.errorHandler(error, dispatch);
    dispatch(updateState('loading', false));
    dispatch(updateState('silentLoading', false));
  }
};
