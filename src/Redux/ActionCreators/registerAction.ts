import { UPDATE_REGISTER_STATE, UPDATE_APP_STATE } from '../Types';
import HttpClient from '../../Utilities/HttpClient';
import Utility from '../../Utilities/Utility';
import DataStore from '../../Utilities/DataStore';

export const updateState = (name: string, value: any) => {
  return {
    type: UPDATE_REGISTER_STATE,
    payload: {
      name,
      value
    }
  };
};

export const registerUser = (
  body: any,
  auth0Response: any,
  rememberMe: boolean
) => async (dispatch: any) => {
  try {
    console.log('Body', body);
    const responseData: any = await HttpClient.register(body);
    console.log('ResponseData', responseData);
    const { data, status } = responseData;
    if (status === 200 || status === 201) {
      Utility.showSuccessToast(data.message);
      dispatch({
        type: UPDATE_APP_STATE,
        payload: {
          name: 'loggedInStatus',
          value: 1
        }
      });
      dispatch({
        type: UPDATE_APP_STATE,
        payload: {
          name: 'userData',
          value: data.data
        }
      });
      if (rememberMe) {
        await DataStore.store('auth0Token', auth0Response.accessToken);
      }
      await DataStore.store('userData', JSON.stringify(data.data));
    }
    dispatch(updateState('loading', false));
    dispatch(updateState('userName', ''));
    dispatch(updateState('email', ''));
    dispatch(updateState('password', ''));
    dispatch(updateState('cnfPassword', ''));
  } catch (error) {
    dispatch(updateState('loading', false));
    if (error.response) {
      Utility.errorHandler(error, dispatch);
    } else {
      Utility.showErrorToast('Failed to connect with server');
    }
  }
};
