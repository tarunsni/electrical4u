import { UPDATE_APP_STATE, UPDATE_PROFILE_STATE } from '../Types';
import HttpClient from '../../Utilities/HttpClient';
import Utility from '../../Utilities/Utility';
import DataStore from '../../Utilities/DataStore';

export const updateState = (name: string, value: any) => {
  return {
    type: UPDATE_PROFILE_STATE,
    payload: {
      name,
      value
    }
  };
};

export const updateUserProfile = (body: any) => async (dispatch: any) => {
  try {
    dispatch(updateState('loading', true));
    const requestBody = {
      dob: body.dob,
      userName: body.userName,
      gender: body.gender,
      location: null,
      phoneNumber: body.phoneNumber
    };
    const responseData = await HttpClient.updateProfile(
      body.userId,
      requestBody
    );
    console.log('ResponseData', responseData);
    const { data, status } = responseData;
    if (status === 200) {
      Utility.showSuccessToast(data.message);
      dispatch({
        type: UPDATE_APP_STATE,
        payload: {
          name: 'userData',
          value: body
        }
      });
      const userData = await DataStore.get('userData');
      if (userData) {
        DataStore.store('userData', JSON.stringify(body));
      }
    }
    dispatch(updateState('loading', false));
  } catch (error) {
    console.log(error);
    console.log(error.response);
    Utility.showErrorToast('Profile updation Failed');
    dispatch(updateState('loading', false));
  }
};
