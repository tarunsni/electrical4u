import { UPDATE_HOME_STATE } from '../Types';
import HttpClient from '../../Utilities/HttpClient';
import Utility from '../../Utilities/Utility';

export const updateState = (name: string, value: any) => {
  return {
    type: UPDATE_HOME_STATE,
    payload: {
      name,
      value
    }
  };
};

export const getSubjectList = () => async (dispatch: any) => {
  try {
    dispatch(updateState('loading', true));
    const responseData = await HttpClient.subjectList();
    console.log('ResponseData', responseData);
    dispatch(updateState('loading', false));
    dispatch(updateState('subjectList', responseData.data.data.subjects || []));
  } catch (error) {
    Utility.errorHandler(error, dispatch);
    dispatch(updateState('loading', false));
  }
};
