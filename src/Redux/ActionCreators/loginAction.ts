import { UPDATE_LOGIN_STATE, UPDATE_APP_STATE } from '../Types';
import HttpClient from '../../Utilities/HttpClient';
import Utility from '../../Utilities/Utility';
import DataStore from '../../Utilities/DataStore';

export const updateState = (name: string, value: any) => {
  return {
    type: UPDATE_LOGIN_STATE,
    payload: {
      name,
      value
    }
  };
};

export const loginUser = (
  body: any,
  auth0Response: any,
  rememberMe: boolean
) => async (dispatch: any) => {
  try {
    console.log('Body', body);
    const responseData: any = await HttpClient.login(body);
    console.log('ResponseData', responseData);
    const { data, status } = responseData;
    if (status === 200 || status === 201) {
      Utility.showSuccessToast(data.message);
      dispatch({
        type: UPDATE_APP_STATE,
        payload: {
          name: 'loggedInStatus',
          value: 1
        }
      });
      dispatch({
        type: UPDATE_APP_STATE,
        payload: {
          name: 'userData',
          value: data.data
        }
      });
      if (rememberMe) {
        DataStore.store('auth0Token', auth0Response.accessToken);
      }
      DataStore.store('userData', JSON.stringify(data.data));
    }
    dispatch(updateState('loading', false));
    dispatch(updateState('email', ''));
    dispatch(updateState('password', ''));
  } catch (error) {
    dispatch(updateState('loading', false));
    if (error.response) {
      Utility.errorHandler(error, dispatch);
    } else {
      Utility.showErrorToast('Failed to connect with server');
    }
  }
};
