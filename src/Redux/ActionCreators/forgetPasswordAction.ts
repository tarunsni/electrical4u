import { UPDATE_FORGET_PASSWORD_STATE } from '../Types';
import HttpClient from '../../Utilities/HttpClient';
import Utility from '../../Utilities/Utility';

export const updateState = (name: string, value: any) => {
  return {
    type: UPDATE_FORGET_PASSWORD_STATE,
    payload: {
      name,
      value
    }
  };
};

export const forgetPassword = (body: any) => async (dispatch: any) => {
  try {
    dispatch(updateState('loading', true));
    console.log('Body', body);
    // const responseData = await HttpClient.forgetPassword(body);
    const responseData = await Utility.resetPassword(body.email);
    console.log('ResponseData', responseData);
    Utility.showSuccessToast(
      'A mail has been sent your email, with password reset instructions.'
    );
    // const { data, status } = responseData;
    // if (status === 200) {
    //   Utility.showSuccessToast(data.message);
    // }
    dispatch(updateState('loading', false));
    // Reset Form
    dispatch(updateState('email', ''));
  } catch (error) {
    Utility.errorHandler(error, dispatch);
    dispatch(updateState('loading', false));
  }
};
