import { ActionInterface } from '../../Interfaces';
import { UPDATE_FORGET_PASSWORD_STATE } from '../Types';

const forgetPasswordReducer = (
  state: any = {
    email: '',
    loading: false
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_FORGET_PASSWORD_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default forgetPasswordReducer;
