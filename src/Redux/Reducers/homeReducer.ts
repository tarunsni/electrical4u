import { ActionInterface } from '../../Interfaces';
import { UPDATE_HOME_STATE } from '../Types';
import HomeStateInterface from '../../Interfaces/Shared/HomeStateInterface';

const homeReducer = (
  state: HomeStateInterface = {
    subjectList: [],
    loading: false
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_HOME_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default homeReducer;
