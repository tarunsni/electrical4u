import { ActionInterface } from '../../Interfaces';
import { UPDATE_PROFILE_STATE } from '../Types';

const profileReducer = (
  state: any = {
    loading: false
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_PROFILE_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default profileReducer;
