import { ActionInterface } from '../../Interfaces';
import { UPDATE_LOGIN_STATE } from '../Types';
import LoginStateInterface from '../../Interfaces/Shared/LoginStateInterface';

const loginReducer = (
  state: LoginStateInterface = {
    email: '',
    password: '',
    rememberMe: true,
    loading: false
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_LOGIN_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default loginReducer;
