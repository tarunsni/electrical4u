import { ActionInterface } from '../../Interfaces';
import { UPDATE_CONTACT_STATE } from '../Types';

const profileReducer = (
  state: any = {
    loading: false,
    name: '',
    email: '',
    phoneNumber: '',
    subject: '',
    message: ''
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_CONTACT_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default profileReducer;
