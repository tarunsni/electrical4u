import { combineReducers } from 'redux';
import appReducer from './appReducer';
import registerReducer from './registerReducer';
import loginReducer from './loginReducer';
import profileReducer from './profileReducer';
import forgetPasswordReducer from './forgetPasswordReducer';
import homeReducer from './homeReducer';
import questionReducer from './questionReducer';
import contactReducer from './contactReducer';

const rootReducer = combineReducers({
  appReducer,
  registerReducer,
  loginReducer,
  profileReducer,
  forgetPasswordReducer,
  homeReducer,
  questionReducer,
  contactReducer
});

export default rootReducer;
