import { ActionInterface } from '../../Interfaces';
import { UPDATE_QUESTION_STATE } from '../Types';

const profileReducer = (
  state: any = {
    loading: false,
    silentLoading: false,
    questionCount: 0,
    currentQuestion: 1,
    questions: []
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_QUESTION_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default profileReducer;
