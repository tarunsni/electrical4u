import { ActionInterface } from '../../Interfaces';
import { UPDATE_REGISTER_STATE } from '../Types';
import RegisterStateInterface from '../../Interfaces/Shared/RegisterStateInterface';

const registerReducer = (
  state: RegisterStateInterface = {
    name: '',
    email: '',
    password: '',
    cnfPassword: '',
    loading: false,
    accountType: 1
  },
  action: ActionInterface
) => {
  switch (action.type) {
    case UPDATE_REGISTER_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default registerReducer;
