import React, { Fragment } from 'react';
import { Platform } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

const KeyboardSpaceriOS = () => {
  return Platform.OS === 'ios' ? (
    <KeyboardSpacer topSpacing={-35} />
  ) : (
    <Fragment />
  );
};

export default KeyboardSpaceriOS;
