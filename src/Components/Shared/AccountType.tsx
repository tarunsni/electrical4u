import React, { Fragment } from 'react';
import { View, Image } from 'react-native';
import Utility from '../../Utilities/Utility';
import { Spacer } from '../../RNMUI';
import Theme from '../../Utilities/Theme';
import { TextComponent } from '..';
import { verticalScale, scale } from 'react-native-size-matters';

interface AccountType {
  type: 1 | 2;
  imageBackground: string;
  accountColor: 'string';
  descColor: 'string';
}

const AccountType = (props: any) => {
  const { type, imageBackground, accountColor, descColor } = props;
  return (
    <Fragment>
      <View
        style={{
          height: Utility.getHeight(180 / 1334),
          width: Utility.getHeight(180 / 1334),
          borderRadius: Utility.getHeight(180 / 1334) / 2,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: imageBackground
        }}
      >
        <Image
          source={
            type == 1
              ? require('../../Assets/medium_battery.png')
              : require('../../Assets/full_battery.png')
          }
          style={{
            height: verticalScale(44),
            width: scale(26)
          }}
          resizeMode={'contain'}
        />
      </View>

      <View>
        <Spacer size={16} />
        <TextComponent
          style={{
            textAlign: 'center',
            color: accountColor,
            fontFamily: Theme.font.bold
          }}
        >
          {type == 1 ? 'FREE ACCOUNT' : 'PREMIUM ACCOUNT'}
        </TextComponent>
        <Spacer size={12} />
        <TextComponent
          size={'h4'}
          style={{
            color: descColor,
            fontWeight: 'normal',
            textAlign: 'center',
            fontFamily: Theme.font.bold
          }}
        >
          {type == 1 ? 'Desc.' : 'Desc.'}
        </TextComponent>
      </View>
    </Fragment>
  );
};

export default AccountType;
