import React from 'react';
import { View, Platform, TextInputProps } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Utility from '../../Utilities/Utility';
import { Card } from '../../RNMUI';
import Theme from '../../Utilities/Theme';
import { verticalScale } from 'react-native-size-matters';

interface InputComponentInterface {
  placeholder: string;
  noBorder?: boolean;
  leftComponent?: any;
  customLeftComponent?: any;
  rightComponent?: any;
  noRadius?: boolean;
  disabled?: boolean;
  value: string;
  inputProps?: TextInputProps;
  onChangeText: Function | any;
  children?: any;
  inputStyle?: any;
}

const InputComponent = (props: InputComponentInterface) => {
  const {
    placeholder,
    noBorder,
    leftComponent,
    rightComponent,
    noRadius,
    value,
    onChangeText,
    inputProps,
    disabled,
    customLeftComponent,
    children,
    inputStyle
  } = props;
  return (
    <Card
      style={{
        flex: 1,
        backgroundColor: Theme.color.white,
        borderBottomColor: Theme.color.light,
        borderBottomWidth: noBorder ? 0 : verticalScale(1),
        flexDirection: 'row',
        borderRadius: noRadius ? 0 : verticalScale(5)
      }}
      elevation={1}
    >
      {customLeftComponent ? (
        customLeftComponent
      ) : (
        <View
          style={{
            flex: 2,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          {leftComponent}
        </View>
      )}
      <View
        style={{ flex: 10, justifyContent: 'center', position: 'relative' }}
      >
        {children ? (
          children
        ) : (
          <TextInput
            style={{
              height: Utility.getHeight(102 / 1334),
              position: 'absolute',
              top: Platform.OS == 'ios' ? verticalScale(1.5) : 0,
              bottom: 0,
              left: 0,
              right: 0,
              fontFamily: Theme.font.regular,
              fontSize: verticalScale(16),
              ...inputStyle
            }}
            editable={!disabled}
            placeholder={placeholder}
            placeholderTextColor={Theme.color.lightShadow}
            value={value}
            onChangeText={onChangeText}
            {...inputProps}
          />
        )}
      </View>
      <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}>
        {rightComponent}
      </View>
    </Card>
  );
};

export default InputComponent;
