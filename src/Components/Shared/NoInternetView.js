import React from 'react';
import { View, ActivityIndicator,Text } from 'react-native';
import Utility from '../../Utilities/Utility';
import { TextComponent } from '../../Components';
import Theme from '../../Utilities/Theme';

const NoInternetView = () => {
  return (
    <View
      style={{
        flex: 1,
        height: Utility.getHeight(1),
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
        <TextComponent
              style={{
                color: Theme.color.dark
              }}
              fontFamily={Theme.font.bold}
              iosMargin>

              No internet
            </TextComponent>
    </View>
  );
};

export default NoInternetView;
