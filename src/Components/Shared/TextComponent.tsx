import React, { StatelessComponent, useContext } from 'react';
import { Text, TextStyle, TextProps, Platform } from 'react-native';
import { verticalScale } from 'react-native-size-matters';
import { ThemeContext } from '../../RNMUI';

interface TitleInterface {
  children?: any;
  style?: TextStyle;
  size?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
  fontFamily?: string;
  titleProps?: TextProps;
  iosMargin?: boolean;
  disableScaling?: boolean;
  singleLine?:boolean;
}

const TextComponent: StatelessComponent<TitleInterface> = props => {
  const {
    children,
    size,
    style,
    titleProps,
    fontFamily,
    singleLine,
    iosMargin,
    disableScaling
  } = props;

  const theme = useContext(ThemeContext);
  const { color } = theme;

  const sizeChoozer = () => {
    let textSize;
    switch (size) {
      case 'h1':
        textSize = 16 * 2;
        break;
      case 'h2':
        textSize = 16 * 1.5;
        break;
      case 'h3':
        textSize = 16 * 1.17;
        break;
      case 'h4':
        textSize = 16;
        break;
      case 'h5':
        textSize = 16 * 0.83;
        break;
      case 'h6':
        textSize = 16 * 0.67;
        break;
      default:
        textSize = 16 * 1.17;
        break;
    }
    return disableScaling ? textSize : verticalScale(textSize);
  };

  return (
    <Text
      numberOfLines={singleLine ? 1 : undefined }
      style={{
        fontSize: sizeChoozer(),
        lineHeight:
          sizeChoozer() +
          (Platform.OS === 'ios' ? verticalScale(6) : verticalScale(5)),
        fontWeight: Platform.OS == 'android' ? '200' : 'bold',
        fontFamily: fontFamily || 'System',
        color: color.dark,
        marginTop: iosMargin
          ? Platform.OS === 'ios'
            ? verticalScale(9)
            : 0
          : 0,
        ...style
      }}
      {...titleProps}
    >
      {children}
    </Text>
  );
};

export default TextComponent;
