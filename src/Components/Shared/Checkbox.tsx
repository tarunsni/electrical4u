import React from 'react';
import { Click } from '../../RNMUI';
import { Image } from 'react-native';
import Utility from '../../Utilities/Utility';
import { verticalScale } from 'react-native-size-matters';

const Checkbox = (props: any) => {
  const { checked, onPress } = props;
  return (
    <Click
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
      }}
      onPress={onPress ? onPress : () => null}
    >
      <Image
        style={{
          height: verticalScale(17),
          width: verticalScale(17)
        }}
        source={
          checked
            ? require('../../Assets/tick.png')
            : require('../../Assets/untick.png')
        }
        resizeMode={'contain'}
      />
    </Click>
  );
};

export default Checkbox;
