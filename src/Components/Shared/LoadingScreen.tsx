import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import Utility from '../../Utilities/Utility';
import Theme from '../../Utilities/Theme';

const LoadingScreen = () => {
  return (
    <View
      style={{
        flex: 1,
        height: Utility.getHeight(1),
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <ActivityIndicator color={Theme.color.primary} size={'large'} />
    </View>
  );
};

export default LoadingScreen;
