import React, { Fragment, useState } from 'react';
import { View, Image, Alert } from 'react-native';
import Utility from '../../Utilities/Utility';
import Theme from '../../Utilities/Theme';
import { InputComponent, TextComponent } from '..';
import { Click } from '../../RNMUI';
// @ts-ignore
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { verticalScale } from 'react-native-size-matters';

const EditProfileForm = (props: any) => {
  const [showPicker, setshowPicker] = useState(false);
  const { userData, updateUserData } = props;
  console.log('userData', userData);
  return (
    <Fragment>
      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Name'}
          noRadius
          leftComponent={
            <Image
              source={require('../../Assets/user.png')}
              style={{
                height: verticalScale(14),
                width: verticalScale(14)
              }}
              resizeMode={'contain'}
            />
          }
          value={userData ? userData.userName : ''}
          onChangeText={(value: string) => updateUserData('userName', value)}
        />
      </View>

      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Email'}
          noRadius
          leftComponent={
            <Image
              source={require('../../Assets/email.png')}
              style={{
                height: verticalScale(11),
                width: verticalScale(15)
              }}
              resizeMode={'contain'}
            />
          }
          disabled
          value={userData ? userData.email : ''}
          onChangeText={(value: string) => {}}
        />
      </View>

      {/* <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Phone Number'}
          noRadius
          inputProps={{
            keyboardType: 'phone-pad'
          }}
          leftComponent={
            <Image
              source={require('../../Assets/cellphone.png')}
              style={{
                height: verticalScale(17),
                width: verticalScale(17)
              }}
              resizeMode={'contain'}
            />
          }
          value={userData ? userData.phoneNumber : ''}
          onChangeText={(value: string) => updateUserData('phoneNumber', value)}
        />
      </View>

      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <Dropdown
          containerStyle={{ flex: 1 }}
          rippleInsets={{ top: 0, bottom: -22 }}
          renderBase={() => (
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View
                style={{
                  flex: 2,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Image
                  source={require('../../Assets/gender.png')}
                  style={{
                    height: verticalScale(15),
                    width: verticalScale(15)
                  }}
                  resizeMode={'contain'}
                />
              </View>

              <View
                style={{
                  flex: 8,
                  height: Utility.getHeight(102 / 1334),
                  justifyContent: 'center',
                  alignItems: 'flex-start'
                }}
              >
                <TextComponent
                  iosMargin
                  fontFamily={Theme.font.regular}
                  size={'h4'}
                  style={{
                    color: userData
                      ? userData.gender != null
                        ? Theme.color.black
                        : Theme.color.lightShadow
                      : Theme.color.lightShadow
                  }}
                >
                  {userData
                    ? userData.gender == '0'
                      ? 'Male'
                      : 'Female'
                    : 'Gender'}
                </TextComponent>
              </View>

              <View
                style={{
                  flex: 2,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Image
                  source={require('../../Assets/dropdown.png')}
                  style={{
                    height: verticalScale(6),
                    width: verticalScale(12)
                  }}
                  resizeMode={'contain'}
                />
              </View>
            </View>
          )}
          onChangeText={(value: string) => updateUserData('gender', value)}
          data={[
            {
              label: 'Male',
              value: '0'
            },
            {
              label: 'Female',
              value: '1'
            }
          ]}
        />
      </View>

      <Click
        onPress={() => setshowPicker(true)}
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image
              source={require('../../Assets/calender.png')}
              style={{
                height: verticalScale(14),
                width: verticalScale(14)
              }}
              resizeMode={'contain'}
            />
          </View>

          <View
            style={{
              flex: 8,
              height: Utility.getHeight(102 / 1334),
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <TextComponent
              iosMargin
              fontFamily={Theme.font.regular}
              size={'h4'}
              style={{
                color: userData
                  ? userData.dob
                    ? Theme.color.black
                    : Theme.color.lightShadow
                  : Theme.color.lightShadow
              }}
            >
              {userData
                ? userData.dob
                  ? new Date(userData.dob).toLocaleDateString()
                  : 'Date of Birth'
                : 'Date of Birth'}
            </TextComponent>
          </View>

          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image
              source={require('../../Assets/dropdown.png')}
              style={{
                height: verticalScale(6),
                width: verticalScale(12)
              }}
              resizeMode={'contain'}
            />
          </View>
        </View>
      </Click>

      <DateTimePicker
        isVisible={showPicker}
        onConfirm={(date: Date) => {
          updateUserData('dob', date.toISOString());
          setshowPicker(false);
        }}
        onCancel={() => setshowPicker(false)}
      /> */}
    </Fragment>
  );
};

export default EditProfileForm;
