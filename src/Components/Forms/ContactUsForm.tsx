import React, { Fragment } from 'react';
import { View, Image } from 'react-native';
import Utility from '../../Utilities/Utility';
import Theme from '../../Utilities/Theme';
import { InputComponent } from '..';
import { verticalScale } from 'react-native-size-matters';

const ContactUsForm = (props: any) => {
  const { userData, updateUserData } = props;
  console.log('userData', userData);
  return (
    <Fragment>
      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Name'}
          noRadius
          leftComponent={
            <Image
              source={require('../../Assets/user.png')}
              style={{
                height: verticalScale(14),
                width: verticalScale(14)
              }}
              resizeMode={'contain'}
            />
          }
          value={userData ? userData.userName : ''}
          onChangeText={(value: string) => updateUserData('userName', value)}
        />
      </View>

      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Email'}
          noRadius
          leftComponent={
            <Image
              source={require('../../Assets/email.png')}
              style={{
                height: verticalScale(11),
                width: verticalScale(15)
              }}
              resizeMode={'contain'}
            />
          }
          disabled
          value={userData ? userData.email : ''}
          onChangeText={(value: string) => {}}
        />
      </View>

      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Phone Number'}
          noRadius
          inputProps={{
            keyboardType: 'phone-pad'
          }}
          leftComponent={
            <Image
              source={require('../../Assets/cellphone.png')}
              style={{
                height: verticalScale(17),
                width: verticalScale(17)
              }}
              resizeMode={'contain'}
            />
          }
          value={userData ? userData.phoneNumber : ''}
          onChangeText={(value: string) => updateUserData('phoneNumber', value)}
        />
      </View>

      <View
        style={{
          minHeight: Utility.getHeight(100 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Subject'}
          noRadius
          leftComponent={
            <Image
              source={require('../../Assets/subject.png')}
              style={{
                height: verticalScale(14),
                width: verticalScale(14)
              }}
              resizeMode={'contain'}
            />
          }
          value={userData ? userData.subject : ''}
          onChangeText={(value: string) => updateUserData('subject', value)}
        />
      </View>

      <View
        style={{
          minHeight: Utility.getHeight(370 / 1334),
          backgroundColor: Theme.color.white,
          flexDirection: 'row',
          borderBottomColor: Theme.color.shadow,
          borderBottomWidth: 0.1,
          paddingHorizontal: 0,
          marginTop: 0.1
        }}
      >
        <InputComponent
          placeholder={'Your Message'}
          inputProps={{ multiline: true }}
          noRadius
          inputStyle={{
            height: Utility.getHeight(370 / 1334),
            marginTop: Utility.getHeight(25 / 1334),
            paddingBottom: Utility.getHeight(25 / 1334)
          }}
          customLeftComponent={
            <View
              style={{
                flex: 2,
                height: Utility.getHeight(100 / 1334),
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={require('../../Assets/message.png')}
                style={{
                  height: verticalScale(14),
                  width: verticalScale(14)
                }}
                resizeMode={'contain'}
              />
            </View>
          }
          value={userData ? userData.message : ''}
          onChangeText={(value: string) => updateUserData('message', value)}
        />
      </View>
    </Fragment>
  );
};

export default ContactUsForm;
