import React from 'react'
import { View, Platform, Image } from 'react-native';
import Utility from '../../Utilities/Utility';
import { Body, Spacer, Card, Click } from '../../RNMUI';
import Theme from '../../Utilities/Theme';
import HTMLView from 'react-native-htmlview';
import HttpClient from '../../Utilities/HttpClient';
import { verticalScale } from 'react-native-size-matters';
import { TextComponent } from '..';

const QuestionRenderer = ({index,renderAnswer,questions,updateQuestionsStatehandler,result,q}:any) => {
  
  let ques = q.que_desc.replace(/'/g, '');
  
  console.log(ques)
  return (
        <View
        style={{ flex: 1, paddingHorizontal: Utility.getWidth(30 / 750) }}
        key={index}>

        <Body
          style={{
            backgroundColor: Theme.color.white,
            paddingHorizontal: Utility.getWidth(30 / 750),
            paddingVertical: Utility.getHeight(30 / 1334)
          }}
        >
          <View>
          <Spacer size={20} />
            <HTMLView
              value={`<div><p>${ques
                }</p></div>`}
              stylesheet={{
                p: {
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  fontWeight: Platform.OS === 'ios' ? '500' : '400',
                  color: Theme.color.black,
                  fontSize: verticalScale(20),
                  textAlign: 'center',
                  fontFamily:
                    Platform.OS === 'ios'
                      ? 'Palatino Linotype'
                      : 'Palatino-Linotype-Bold'
                },
                div: {
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  textAlign: 'center'
                }
              }}
            />
          </View>
          <Spacer size={20} />
          <View style={{ paddingHorizontal: Utility.getWidth(25 / 750) }}>
            {renderAnswer(questions, index, q, '1')}
            {renderAnswer(questions, index, q, '2')}
            {renderAnswer(questions, index, q, '3')}
            {renderAnswer(questions, index, q, '4')}
          </View>
          {result(q)}
          <View style={{ alignItems: 'center' }}>
            <Card
              style={{
                width: Utility.getWidth(280 / 750),
                height: Utility.getHeight(100 / 1334),
                backgroundColor: Theme.color.primary,
                borderRadius: 5
              }}
            >
              <Click
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row'
                }}
                onPress={() => {
                    updateQuestionsStatehandler()
                }}
              >
                <Image
                  source={require('../../Assets/hint.png')}
                  style={{
                    height: Utility.getWidth(60 / 750),
                    width: Utility.getWidth(60 / 750)
                  }}
                />
                <TextComponent> </TextComponent>
                <TextComponent
                  fontFamily={Theme.font.bold}
                  size={'h3'}
                  style={{
                    color: Theme.color.light,
                    marginTop:
                      Platform.OS === 'ios'
                        ? Utility.getHeight(20 / 1334)
                        : 0
                  }}
                >
                  HINTS
                </TextComponent>
              </Click>
            </Card>
          </View>
        </Body>
      </View>
    
    )
}

export default QuestionRenderer
