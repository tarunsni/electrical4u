import React, {Component,Fragment} from 'react'
import {View, Platform} from 'react-native'
import { Click, Spacer } from '../../RNMUI';
import Utility from '../../Utilities/Utility';
import Theme from '../../Utilities/Theme';
import { verticalScale } from 'react-native-size-matters';
import HTMLView from 'react-native-htmlview';

const QuestionComponent = ({q,questions,index,userSelectedAnswer,updateAnswerHandler}:any) => {
    return (
        <Fragment>
        <Click
          style={{
            minHeight: Utility.getHeight(90 / 1334),
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 2,
            borderRadius: 5,
            borderColor:
              q.user_ans == q.true_ans && q.user_ans == userSelectedAnswer
                ? Theme.color.borderGreen
                : q.user_ans == userSelectedAnswer
                ? Theme.color.borderPink
                : Theme.color.lightOverlay,
            backgroundColor:
              q.user_ans == q.true_ans && q.user_ans == userSelectedAnswer
                ? Theme.color.lightGreen
                : q.user_ans == userSelectedAnswer
                ? Theme.color.lightPink
                : Theme.color.white,
            overflow: 'hidden',
            paddingTop: Platform.OS === 'ios' ? verticalScale(10) : 5,
            paddingHorizontal: 5,
            paddingBottom: 5
          }}
          onPress={() => {
            updateAnswerHandler(questions,index,userSelectedAnswer)
          }}
          clickProps={{ useForeground: true }}
        >
          <HTMLView
            value={`<div><p>${q['ans' + userSelectedAnswer]
              .charAt(0)
              .toUpperCase() +
              q['ans' + userSelectedAnswer].slice(1)}</p></div>`}
            stylesheet={{
              p: {
                position: 'absolute',
                top: 0,
                bottom: 0,
                fontWeight: '400',
                fontSize: 20,
                textAlign: 'center',
                color:
                  q.user_ans == q.true_ans && q.user_ans == userSelectedAnswer
                    ? Theme.color.success
                    : q.user_ans == userSelectedAnswer
                    ? Theme.color.danger
                    : Theme.color.dark,
                fontFamily:
                  Platform.OS === 'ios'
                    ? 'Palatino Linotype'
                    : 'Palatino-Linotype-Regular'
              }
            }}
          />
        </Click>
        <Spacer />
      </Fragment>
    )
}

export default QuestionComponent
