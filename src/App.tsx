import React from 'react';
import AppRouter from './Navigations/AppRouter';
import { ThemeProvider } from './RNMUI';
import { Provider } from 'react-redux';
import Store from './Redux/Store';
import Theme from './Utilities/Theme';

const App = () => {
  return (
    <Provider store={Store}>
      <ThemeProvider value={Theme}>
        <AppRouter />
      </ThemeProvider>
    </Provider>
  );
};

export default App;
