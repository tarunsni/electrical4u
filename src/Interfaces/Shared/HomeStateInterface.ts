interface HomeStateInterface {
  subjectList: Array<any>;
  loading: boolean;
}

export default HomeStateInterface;
