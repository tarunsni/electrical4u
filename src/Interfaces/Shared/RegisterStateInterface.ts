interface RegisterStateInterface {
  name: string;
  email: string;
  password: string;
  cnfPassword: string;
  loading: boolean;
  accountType: number;
}

export default RegisterStateInterface;
