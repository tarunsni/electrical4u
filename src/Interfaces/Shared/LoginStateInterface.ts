interface LoginStateInterface {
  email: string;
  password: string;
  rememberMe: boolean;
  loading: boolean;
}

export default LoginStateInterface;
